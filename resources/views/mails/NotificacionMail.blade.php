<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>SAM</title>
</head>
<body>
    @if($data->Estado === "Abierto")
    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="max-width:768px">
        <tbody>
            <tr>
                <td>
                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="max-width:670px;border:solid 1px #dcdcdc; background-color: #fff">
                        <tbody>
                            <tr>
                                <td style="text-align:center">
                                    <img src="https://i.imgur.com/cWbcp2z.png" style="width:100%;max-width:40%;padding:20px 0px 0px;border:0" alt class="CToWUd">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0px 10%">
                                    <table width="100%" align="center" cellspacing="0" cellpadding="0" border="0" style="max-width:500px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3;color:#8e8e8e;text-align:center">
                                        <tbody>
                                            <tr>
                                                <td width="100%" style="padding:20px 10% 15px">
                                                    <span style="font-size:20px;font-weight:bold;color:#210049">Solicitud del usuario {{$data->NombreCompleto}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" style="padding:0px 0px 30px;text-align:center">
                                                    Su solicitud a sido enviada, el caso fue tomado como el <b>#{{$data->id_Solicitud}}</b>, esperamos darle pronta solucion al incidente, gracias por su atencion.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;max-width:670px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    @endif
    @if($data->Estado === "Asignado")
    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="max-width:768px">
        <tbody>
            <tr>
                <td>
                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="max-width:670px;border:solid 1px #dcdcdc; background-color: #fff">
                        <tbody>
                            <tr>
                                <td style="text-align:center">
                                    <img src="https://i.imgur.com/cWbcp2z.png" style="width:100%;max-width:40%;padding:20px 0px 0px;border:0" alt class="CToWUd">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0px 10%">
                                    <table width="100%" align="center" cellspacing="0" cellpadding="0" border="0" style="max-width:500px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3;color:#8e8e8e;text-align:center">
                                        <tbody>
                                            <tr>
                                                <td width="100%" style="padding:20px 10% 15px">
                                                    <span style="font-size:20px;font-weight:bold;color:#210049">Solicitud del usuario {{$data->NombreCompleto}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" style="padding:0px 0px 30px;text-align:center">
                                                    Su solicitud a sido tomada por el tecnico <b>{{$data->NombreTec}}</b>, esperamos darle pronta solucion al incidente, gracias por su atencion.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;max-width:670px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    @endif
    @if($data->Estado === "Verificado")
    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="max-width:768px">
        <tbody>
            <tr>
                <td>
                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="max-width:670px;border:solid 1px #dcdcdc; background-color: #fff">
                        <tbody>
                            <tr>
                                <td style="text-align:center">
                                    <img src="https://i.imgur.com/cWbcp2z.png" style="width:100%;max-width:40%;padding:20px 0px 0px;border:0" alt class="CToWUd">
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:0px 10%">
                                    <table width="100%" align="center" cellspacing="0" cellpadding="0" border="0" style="max-width:500px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:16px;line-height:1.3;color:#8e8e8e;text-align:center">
                                        <tbody>
                                            <tr>
                                                <td width="100%" style="padding:20px 10% 15px">
                                                    <span style="font-size:20px;font-weight:bold;color:#210049">Solicitud del usuario {{$data->NombreCompleto}}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100%" style="padding:0px 0px 30px;text-align:center">
                                                    Nos alegra informarle de que, la solicitud tomada por el técnico <b>{{$data->NombreTec}}</b>, ha sido resuelta y su caso ya ha sido verificado, gracias por su atención y paciencia.
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;max-width:670px">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    @endif
</body>
</html>