<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>SAM</title>
</head>
<body>
	<style>
		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: #5D6975;
			text-decoration: underline;
		}

		body {
			position: relative;
			width: 21cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #001028;
			background: #FFFFFF; 
			font-family: Arial, sans-serif; 
			font-size: 12px; 
			font-family: Arial;
		}

		header {
			padding: 10px 0;
			margin-bottom: 30px;
		}

		#logo {
			text-align: center;
			margin-bottom: 10px;
		}

		#logo img {
			width: 40%;
		}

		h1 {
			border-top: 1px solid  #5D6975;
			border-bottom: 1px solid  #5D6975;
			color: #5D6975;
			font-size: 2.4em;
			line-height: 1.4em;
			font-weight: normal;
			text-align: center;
			margin: 0 0 20px 0;
			background: url(dimension.png);
		}

		table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		table tr:nth-child(2n-1) td {
			background: #F5F5F5;
		}

		table th,
		table td {
			text-align: center;
		}

		table th {
			padding: 5px 20px;
			color: #5D6975;
			border-bottom: 1px solid #C1CED9;
			white-space: nowrap;        
			font-weight: normal;
		}

		table .desc {
			text-align: left;
		}

		table td {
			padding: 20px;
			text-align: right;
		}

		table td.desc {
			vertical-align: top;
		}

		table td.unit,
		table td.qty,
		table td.total {
			font-size: 1.2em;
		}

		table td.grand {
			border-top: 1px solid #5D6975;;
		}

		footer {
			color: #5D6975;
			width: 100%;
			height: 30px;
			position: absolute;
			bottom: 0;
			border-top: 1px solid #C1CED9;
			padding: 8px 0;
			text-align: center;
		}
	</style>
	<header class="clearfix">
		<div id="logo">
			<img src="C:/xampp/htdocs/SAMV3/public/img/SAM1000x1000.png">
		</div>
		<h1>Reporte SAM</h1>
	</header>
	<main>
		<table>
			<thead>
				<tr>
					<th class="desc">TECNICOS</th>
					<th>CASOS ASIGNADOS</th>
					<th>CASOS CERRADOS</th>
					<th>TOTAL CASOS</th>
				</tr>
			</thead>
			<tbody>
				@foreach($datos as $solicitud)
				<tr>
					<td class="desc">{{$solicitud->NombreCom}}</td>
					<td class="unit"></td>
					<td class="qty"></td>
					<td class="total"></td>
				</tr>
				@endforeach
				<tr>
					<td colspan="4" class="grand total" style="margin-right: 50% !important">CASOS TOTALES</td>
					<td class="grand total" style="margin-right: 70% !important">{{$casoTotal}}</td>
				</tr>
			</tbody>
		</table>
	</main>
	<footer>
		
	</footer>
</body>
</html>