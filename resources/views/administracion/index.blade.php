@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/datatable.css')}}">
<link rel="stylesheet" href="{{asset('css/modalStyle.css')}}">
<link rel="stylesheet" href="{{asset('css/administracion.css')}}">
<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10 text-center">
			<h1><strong>ADMINISTRAR</strong></h1>
			<div class="row justify-content-center card-m">
				<div class="col-4">
					<div class="card card-bg">
						<div class="card-body"><img src="{{ asset('img/music-album.svg') }}" class="w-50"></div>
						<h4><strong>CATEGORÍAS</strong></h4>
						<div class="col-12">
							<a href="#" class="btn btn-lis" data-toggle="modal" data-target="#ListCategoModal">Listar</a>
							<a href="#" class="btn btn-reg" data-toggle="modal" data-target="#CategoriaModal">Registrar</a>
						</div>						
					</div>
				</div>
				<div class="col-4">
					<div class="card card-bg">
						<div class="card-body"><img src="{{ asset('img/edifice.svg') }}" class="w-50"></div>
						<h4><strong>EDIFICIOS</strong></h4>
						<div class="col-12">
							<a href="#" class="btn btn-lis" data-toggle="modal" data-target="#ListEdifiModal">Listar</a>
							<a href="#" class="btn btn-reg" data-toggle="modal" data-target="#EdificioModal">Registrar</a>
						</div>						
					</div>
				</div>
				<div class="col-4">
					<div class="card card-bg">
						<div class="card-body"><img src="{{ asset('img/upstairs.svg') }}" class="w-50"></div>
						<h4><strong>PISOS</strong></h4>
						<div class="col-12">
							<a href="#" class="btn btn-lis" data-toggle="modal" data-target="#ListPisosModal">Listar</a>
							<a href="#" class="btn btn-reg" data-toggle="modal" data-target="#PisosModal">Registrar</a>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="card card-bg">
						<div class="card-body"><img src="{{ asset('img/doorway.svg') }}" class="w-50"></div>
						<h4><strong>ESPACIOS</strong></h4>
						<div class="col-12">
							<a href="#" class="btn btn-lis" data-toggle="modal" data-target="#ListEspacioModal">Listar</a>
							<a href="#" class="btn btn-reg" data-toggle="modal" data-target="#EspacioModal">Registrar</a>
						</div>						
					</div>
				</div>
				<div class="col-4">
					<div class="card card-bg">
						<div class="card-body"><img src="{{ asset('img/reloj.svg') }}" class="w-50"></div>
						<h4><strong>PRIORIDAD</strong></h4>
						<div class="col-12">
							<a href="#" class="btn btn-lis" data-toggle="modal" data-target="#ListTimeModal">Listar</a>
						</div>						
					</div>
				</div>
				<div class="col-4">
					<div class="card card-bg">
						<div class="card-body"><img src="{{ asset('img/insumo.svg') }}" class="w-50"></div>
						<h4><strong>INSUMOS</strong></h4>
						<div class="col-12">
							<a href="#" class="btn btn-lis" data-toggle="modal" data-target="#ListInsumoModal">Listar</a>
							<a href="#" class="btn btn-reg" data-toggle="modal" data-target="#InsumoModal">Registrar</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{-- Modal Registrar Categorias --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="CategoriaModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- formulario categoria --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>REGISTRAR NUEVA CATEGORIA</h1>
					</div>
					<div class="modal-body">
						<form class="signup-form" id="categoriaValidate" method="POST" action="{{ route('regisCategoria') }}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Nombre" class="col-md-12 form-label text-md-left">Nombre de la categoria<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Nombre" type="text" class="form-control" name="Nombre" value="{{ old('Nombre') }}" autocomplete="Nombre" autofocus placeholder="Ingrese la categoria"
										onkeyup="this.value=CampoLetras(this.value)">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		{{-- Modal Registrar Edificios --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="EdificioModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- formulario Edificios --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>REGISTRAR NUEVO EDIFICIO</h1>
					</div>
					<div class="modal-body">
						<form class="signup-form" id="edificioValidate" method="POST" action="{{ route('regisEdificio') }}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Nombre" class="col-md-12 form-label text-md-left">Nombre del edificio<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Nombre" type="text" class="form-control" name="Nombre" value="{{ old('Nombre') }}" autocomplete="Nombre" autofocus placeholder="Ingrese el edificio">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		{{-- Modal Registrar Pisos --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="PisosModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- formulario Piso --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>REGISTRAR NUEVO PISO</h1>
					</div>
					<div class="modal-body">
						<form class="signup-form" id="pisoValidate" method="POST" action="{{ route('regisPiso') }}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Piso" class="col-md-12 form-label text-md-left">Número del piso<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Piso" type="text" class="form-control" name="Piso" value="{{ old('Piso') }}" autocomplete="Piso" autofocus placeholder="Ejemplo 'Piso 1'">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		{{-- Modal Registrar Espacio --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="espacioModalLabel" id="EspacioModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- formulario Espacio --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>REGISTRAR NUEVO ESPACIO</h1>
					</div>
					<div class="modal-body">
						<form class="signup-form" id="espacioValidate" method="POST" action="{{ route('regisEspacio') }}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Espacio" class="col-md-12 form-label text-md-left">Espacio<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Espacio" type="text" class="form-control" name="Espacio" value="{{ old('Espacio') }}" autocomplete="Espacio" autofocus placeholder="Ambiente 101">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		{{-- Modal Registrar Insumos --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="insumoModalLabel" id="InsumoModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- formulario Insumo --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>REGISTRAR NUEVO INSUMO</h1>
					</div>
					<div class="modal-body">
						<form class="signup-form" id="insumoValidate" method="POST" action="{{ route('regisInsumo') }}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-6">
									<label for="Insumo" class="col-md-12 form-label text-md-left">Insumo<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Insumo" type="text" class="form-control" name="Insumo" value="{{ old('Insumo') }}" autocomplete="Insumo" autofocus placeholder="Ingrese el nombre del insumo">
									</div>
								</div>
								<div class="col-6">
									<label for="Cantidad" class="col-md-12 form-label text-md-left">Cantidad<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Cantidad" type="number" class="form-control" name="Cantidad" value="{{ old('Cantidad') }}" autocomplete="Cantidad" autofocus placeholder="Ingrese la cantidad de insumos">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		{{-- Modal Listar Categorias --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="ListCategoModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="justify-content: center;">
						<h1>VER TODAS LAS CATEGORIAS</h1>
					</div>
					<div class="modal-body">
						<table id="listCatego" class="table table-light table-striped">
							<thead class="thead-color">
								<tr>
									<th class="text-center th-cl">N°</th>
									<th class="text-center">Categoria</th>
									<th class="text-center th-cr">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($categorias as $catego)
								<tr>
									<td class="text-center">{{$i++}}</td>
									<td class="text-center">{{$catego->Nombre}}</td>
									<td class="text-center">
										<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditCatego{{$catego->id_Categoria}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		@foreach($categorias as $catego)
		{{-- Modal Editar Categorias  --}}
		<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="EditCatego{{$catego->id_Categoria}}" style="top: 20% !important; left: 0 !important;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- Formulario Editar Categorias --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>EDITAR CATEGORIA</h1>
					</div>
					<div class="modal-body">
						<form class="editarCategoria{{$catego->id_Categoria}}" id="editarCategoria" method="POST" action="{{route('categoriaUpdate',[$catego->id_Categoria])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Nombre" class="col-md-12 form-label text-md-left">Nombre de la categoria<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Nombre" type="text" class="form-control" name="Nombre" value="{{$catego->Nombre}}" autocomplete="Nombre" autofocus placeholder="Ingrese el nombre de la categoria"
										onkeyup="this.value=CampoLetras(this.value)">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- fin del modal --}}
		@endforeach

		{{-- Modal Listar Edificios --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="ListEdifiModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="justify-content: center;">
						<h1>VER TODOS LOS EDIFICIOS</h1>
					</div>
					<div class="modal-body">
						<table id="listEdific" class="table table-light table-striped">
							<thead class="thead-color">
								<tr>
									<th class="text-center th-cl">N°</th>
									<th class="text-center">Edificios</th>
									<th class="text-center th-cr">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($edificios as $edific)
								<tr>
									<td class="text-center">{{$e++}}</td>
									<td class="text-center">{{$edific->Nombre}}</td>
									<td class="text-center">
										<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditEdifi{{$edific->id_Edificio}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		@foreach($edificios as $edific)
		{{-- Modal Editar Edificio  --}}
		<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="EditEdifi{{$edific->id_Edificio}}" style="top: 20% !important; left: 0 !important;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- Formulario Editar Edificio --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>EDITAR EDIFICIO</h1>
					</div>
					<div class="modal-body">
						<form class="editarEdificio{{$edific->id_Edificio}}" id="editarEdificio" method="POST" action="{{route('edificioUpdate',[$edific->id_Edificio])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Nombre" class="col-md-12 form-label text-md-left">Nombre del edificio<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Nombre" type="text" class="form-control" name="Nombre" value="{{$edific->Nombre}}" autocomplete="Nombre" autofocus placeholder="Ingrese el nombre del edificio">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- fin del modal --}}
		@endforeach

		{{-- Modal Listar Pisos --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="ListPisosModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="justify-content: center;">
						<h1>VER TODOS LOS PISOS</h1>
					</div>
					<div class="modal-body">
						<table id="ListPisos" class="table table-light table-striped">
							<thead class="thead-color">
								<tr>
									<th class="text-center th-cl">N°</th>
									<th class="text-center">Pisos</th>
									<th class="text-center th-cr">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($pisos as $piso)
								<tr>
									<td class="text-center">{{$u++}}</td>
									<td class="text-center">{{$piso->Piso}}</td>
									<td class="text-center">
										<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditPiso{{$piso->id_Piso}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		@foreach($pisos as $piso)
		{{-- Modal Editar Pisos  --}}
		<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="EditPiso{{$piso->id_Piso}}" style="top: 20% !important; left: 0 !important;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- Formulario Editar Piso --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>EDITAR PISO</h1>
					</div>
					<div class="modal-body">
						<form class="editarPiso{{$piso->id_Piso}}" id="editarPiso" method="POST" action="{{route('pisoUpdate',[$piso->id_Piso])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Piso" class="col-md-12 form-label text-md-left">Número del piso<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Piso" type="text" class="form-control" name="Piso" value="{{$piso->Piso}}" autocomplete="Piso" autofocus placeholder="Ingrese el número del piso">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- fin del modal --}}
		@endforeach

		{{-- Modal Listar Espacios --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="categoriaModalLabel" id="ListEspacioModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="justify-content: center;">
						<h1>VER TODOS LOS ESPACIOS</h1>
					</div>
					<div class="modal-body">
						<table id="ListEspacios" class="table table-light table-striped">
							<thead class="thead-color">
								<tr>
									<th class="text-center th-cl">N°</th>
									<th class="text-center">Espacio</th>
									<th class="text-center th-cr">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($espacio as $espacios)
								<tr>
									<td class="text-center">{{$w++}}</td>
									<td class="text-center">{{$espacios->Espacio}}</td>
									<td class="text-center">
										<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#Editespacio{{$espacios->id_Espacio}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		@foreach($espacio as $espacios)
		{{-- Modal Editar Espacio  --}}
		<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="espacioModalLabel" id="Editespacio{{$espacios->id_Espacio}}" style="top: 20% !important; left: 0 !important;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- Formulario Editar Espacio --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>EDITAR PISO</h1>
					</div>
					<div class="modal-body">
						<form class="editEspacio{{$espacios->id_Espacio}}" id="editarPiso" method="POST" action="{{route('espacioUpdate',[$espacios->id_Espacio])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Espacio" class="col-md-12 form-label text-md-left">Espacio<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Espacio" type="text" class="form-control" name="Espacio" value="{{$espacios->Espacio}}" autocomplete="Espacio" autofocus placeholder="Ambiente 101">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- fin del modal --}}
		@endforeach

		{{-- Modal Listar Insumos --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="insumosModalLabel" id="ListInsumoModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="justify-content: center;">
						<h1>VER TODOS LOS INSUMOS</h1>
					</div>
					<div class="modal-body">
						<table id="ListInsumos" class="table table-light table-striped">
							<thead class="thead-color">
								<tr>
									<th class="text-center th-cl">N°</th>
									<th class="text-center">Insumo</th>
									<th class="text-center">Cantidad</th>
									<th class="text-center th-cr">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($insumo as $insumos)
								<tr>
									<td class="text-center">{{$q++}}</td>
									<td class="text-center">{{$insumos->Insumo}}</td>
									<td class="text-center">{{$insumos->Cantidad}}</td>
									<td class="text-center">
										<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditInsumo{{$insumos->id_Insumo}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}

		@foreach($insumo as $insumos)
		{{-- Modal Editar Insumo  --}}
		<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="EditInsumo{{$insumos->id_Insumo}}" style="top: 20% !important; left: 0 !important;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- Formulario Editar Piso --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>EDITAR INSUMO</h1>
					</div>
					<div class="modal-body">
						<form class="editarInsumo{{$insumos->id_Insumo}}" method="POST" action="{{route('insumoUpdate',[$insumos->id_Insumo])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-6">
									<label for="Insumo" class="col-md-12 form-label text-md-left">Insumo<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Insumo" type="text" class="form-control" name="Insumo" value="{{$insumos->Insumo}}" autocomplete="Insumo" autofocus placeholder="Ingrese el nombre del insumo">
									</div>
								</div>
								<div class="col-6">
									<label for="Cantidad" class="col-md-12 form-label text-md-left">Cantidad<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Cantidad" type="number" class="form-control" name="Cantidad" value="0" autocomplete="Cantidad" autofocus placeholder="Ingrese la cantidad de insumos">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- fin del modal --}}
		@endforeach

		{{-- Modal Listar Tiempos --}}
		<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="timeModalLabel" id="ListTimeModal">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="justify-content: center;">
						<h1>VER TODAS LAS PRIORIDADES</h1>
					</div>
					<div class="modal-body">
						<table id="ListPrioridad" class="table table-light table-striped">
							<thead class="thead-color">
								<tr>
									<th class="text-center th-cl">N°</th>
									<th class="text-center">Prioridad</th>
									<th class="text-center">Tiempo</th>
									<th class="text-center th-cr">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@foreach($prioridad as $prioridades)
								<tr>
									<td class="text-center">{{$p++}}</td>
									<td class="text-center">{{$prioridades->Prioridad}}</td>
									<td class="text-center">{{$prioridades->Tiempo}} Horas</td>
									<td class="text-center">
										<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditTime{{$prioridades->id_Prioridad}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
					</div>
				</div>
			</div>
		</div>
		{{-- Fin Modal --}}
		
		@foreach($prioridad as $prioridades)
		{{-- Modal Registrar Tiempos  --}}
		<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="EditTime{{$prioridades->id_Prioridad}}" style="top: 13% !important; left: 0 !important;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					{{-- Formulario Registrar Tiempos --}}
					<div class="modal-header" style="justify-content: center;">
						<h1>REGISTRAR TIEMPO DE PRIORIDAD <span class="text-tr">{{$prioridades->Prioridad}}</span></h1>
					</div>
					<div class="modal-body">
						<form id="tiempoValidate{{$prioridades->id_Prioridad}}" method="POST" action="{{route('prioridadUpdate',[$prioridades->id_Prioridad])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="row justify-content-center">
								<div class="col-8">
									<label for="Tiempo" class="col-md-12 form-label text-md-left">Tiempo de la prioridad<span class="red">*</span></label>
									<div class="col-md-12">
										<input id="Tiempo" type="number" min="0" max="744" class="form-control" name="Tiempo" value="{{$prioridades->Tiempo}}" autocomplete="Tiempo" autofocus placeholder="Ingrese el tiempo de la prioridad">
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		{{-- fin del modal --}}
		@endforeach
	</div>
</div>
{{-- Jquery para validar --}}
<script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('js/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('js/minimalist-picker/dobpicker.js')}}"></script>
<script src="{{asset('js/jquery.pwstrength/jquery.pwstrength.js')}}"></script>

{{-- Validaciones --}}
<script src="{{asset('js/validaciones/mainCategoria.js')}}"></script>
<script src="{{asset('js/validaciones/mainEdificio.js')}}"></script>
<script src="{{asset('js/validaciones/mainPiso.js')}}"></script>
<script src="{{asset('js/validaciones/mainPrioridad.js')}}"></script>
<script src="{{asset('js/validaciones/mainEspacio.js')}}"></script>
<script src="{{asset('js/validaciones/mainInsumo.js')}}"></script>

{{-- Scrips abrir datatables --}}
<script>
	$(document).ready(function() {
		$('#listCatego').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$('#listEdific').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$('#ListPisos').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$('#ListInsumos').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
<script>
	$(document).ready(function() {
		$('#ListEspacios').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
@foreach($categorias as $catego)
<script>
	(function($) {
		var form = $(".editarCategoria{{$catego->id_Categoria}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			Nombre: {
				required: true,
				letterswithbasicpunc: true,
			},
		},
		messages : {
			Nombre: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				letterswithbasicpunc: 'Solo letras y sin espacios <i class="fas fa-exclamation-triangle"></i>'
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});

	})(jQuery);
</script>
@endforeach
@foreach($edificios as $edific)
<script>
	(function($) {
		var form = $(".editarEdificio{{$edific->id_Edificio}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			Nombre: {
				required: true,
			},
		},
		messages : {
			Nombre: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});
	})(jQuery);
</script>
@endforeach
@foreach($pisos as $piso)
<script>
	(function($) {
		var form = $(".editarPiso{{$piso->id_Piso}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			Piso: {
				required: true,
			},
		},
		messages : {
			Piso: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});
	})(jQuery);
</script>
@endforeach
@foreach($espacio as $espacios)
<script>
	(function($) {
		var form = $(".editEspacio{{$espacios->id_Espacio}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			Espacio: {
				required: true,
			},
		},
		messages : {
			Espacio: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});
	})(jQuery);
</script>
@endforeach
@foreach($insumo as $insumos)
<script>
	(function($) {
		var form = $(".editarInsumo{{$insumos->id_Insumo}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			Insumo: {
				required: true,
			},
			Cantidad: {
				required: true,
				number: true,
				min: 0,
			},
		},
		messages : {
			Insumo: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Cantidad: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Solo números enteros <i class="fas fa-exclamation-triangle"></i>',
				min: 'La cantidad debe ser mayor o igual a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});
	})(jQuery);
</script>
@endforeach
@endsection