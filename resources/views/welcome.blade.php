<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="/img/favicon.png">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/file-input.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styleValidate.css')}}" rel="stylesheet">
    <title>SAM</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-lg-12">
                <div class="col-6 float-right">
                    <div>
                        <ul id="menu">
                            <li><a href="#" class="active" style="border-radius: 30px 0 0 0;"><strong>Registrar
                                        solicitud</strong> </a></li>
                            <li><a href="#" style="border-radius: 0 30px 0 0;"><strong>Ingresar</strong></a></li>
                        </ul>
                    </div>
                    <div class="col-12 logStyle">
                        <div id="formularios">
                            <form id="SolicitudForm" method="POST" enctype="multipart/form-data"
                                action="{{ route('solicitudMante')}}">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}
                                <h4 class="titleMante"><strong>Solicitud de Mantenimiento</strong></h4>
                                <div class="form-group col-sm-12">
                                    <label>Nombre completo<span class="red">*</span></label>
                                    <input onkeyup="this.value=CampoLetras(this.value)" type="text" class="form-control"
                                        name="NombreCompleto" id="NombreCompleto" placeholder="Ingresa nombre completo"
										onkeyup="this.value=CampoLetras(this.value)">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Correo electronico<span class="red">*</span></label>
                                    <input type="email" class="form-control" name="Email" id="Email"
                                        placeholder="Ejemplo@gmail.com">
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Prioridad<span class="red">*</span></label>
                                    <select class="form-control" name="Prioridad" id="Prioridad">
                                        <option value="">Seleccionar</option>
                                        @foreach($prioridad as $prioridades)
                                        <option value="{{$prioridades->id_Prioridad}}">{{$prioridades->Prioridad}}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Categoria<span class="red">*</span></label>
                                    <select class="form-control" name="Categoria" id="Categoria">
                                        <option value="">Seleccionar</option>
                                        @foreach($categorias as $categoria)
                                        <option value="{{$categoria->id_Categoria}}">{{$categoria->Nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Edificio<span class="red">*</span></label>
                                    <select class="form-control" name="Edificio" id="Edificio">
                                        <option value="">Seleccionar</option>
                                        @foreach($edificios as $edificio)
                                        <option value="{{$edificio->id_Edificio}}">{{$edificio->Nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Piso<span class="red">*</span></label>
                                    <select class="form-control" name="Piso" id="Piso">
                                        <option value="">Seleccionar</option>
                                        @foreach($pisos as $piso)
                                        <option value="{{$piso->id_Piso}}">{{$piso->Piso}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Espacio<span class="red">*</span></label>
                                    <select class="form-control" name="Espacio" id="Espacio">
                                        <option value="">Seleccionar</option>
                                        @foreach($espacio as $espacios)
                                        <option value="{{$espacios->id_Espacio}}">{{$espacios->Espacio}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Descripción<span class="red">*</span></label>
                                    <textarea type="text" class="form-control" name="Descripcion" id="Descripcion"
                                        rows="5"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group col-6 float-left text-right">
                                            <input type="file" name="Imagen[]" id="file-2" class="inputfile inputfile-2"
                                                data-multiple-caption="{count} Imagenes seleccionadas" multiple
                                                accept="image/*" />
                                            <label for="file-2">
                                                <span class="iborrainputfile">Adjuntar imagen</span>
                                            </label>
                                        </div>
                                        <div class="form-group col-6 float-right">
                                            <button type="submit" class="btn btn-env">Enviar Solicitud</button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="Estado" value="Abierto">
                                <input type="hidden" name="Fecha_solicitud" value="{{$carbon->now()->format('d/m/Y')}}"
                                    readonly>
                            </form>
                            {{-- Inicio de sesion --}}
                            <form class="form-horizontal" id="login" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <h4 class="titleLogin"><strong>Ingresar</strong></h4>
                                <div class="form-group">
                                    <label for="email" class="col-md-6 control-label"><b>Usuario</b></label>
                                    <div class="col-md-12">
                                        <input id="email" type="text" class="form-control" name="email"
                                            value="{{ old('email') }}" autofocus placeholder="Ingresa tu Usuario">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-4 control-label"><b>Contraseña</b></label>
                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control" name="password"
                                            placeholder="Ingresa tu Contraseña">
                                    </div>
                                </div>
                                <div class="row" style="padding-bottom: 15%;">
                                    <div class="col-md-12 form-group text-center">
                                        <button type="submit" class="btn btn-env btn-txt">Entrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/beda2d4e59.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/jquery-validation/dist/additional-methods.min.js')}}"></script>
    <script src="{{asset('js/jquery-steps/jquery.steps.min.js')}}"></script>
    <script src="{{asset('js/minimalist-picker/dobpicker.js')}}"></script>
    <script src="{{asset('js/jquery.pwstrength/jquery.pwstrength.js')}}"></script>
    <script src="{{asset('js/custom-file-input.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{-- Validaciones --}}
    <script src="{{asset('js/validaciones/mainLogin.js')}}"></script>
    <script src="{{asset('js/validaciones/mainSolicitud.js')}}"></script>
    {{-- Fin validaciones --}}
    <script>
        $(function () {
            $("#menu li").on("click", function () {
                var i = $(this).index();
                $("#formularios form").hide();
                $("#formularios form").eq(i).show();
                $("#menu li a").removeClass("active");
                $(this).find("a").addClass("active");
            });
        });

        $(document).ready(function () {
            window.location.hash = "login";
            window.location.hash = "Again-No-back-button"; //esta linea es necesaria para chrome
            window.onhashchange = function () {
                window.location.hash = "login";
            }
        });
		
		   function CampoLetras(string) { 
    var out = '';
    //Se añaden las letras validas
    var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ '; //Caracteres validos

    for (var i = 0; i < string.length; i++)
        if (filtro.indexOf(string.charAt(i)) != -1)
            out += string.charAt(i);
    return out;
}

    </script>
    @include ('sweet::alert')
</body>

</html>
