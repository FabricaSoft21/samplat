@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<link rel="stylesheet" href="{{asset('css/datatable.css')}}">
<link rel="stylesheet" href="{{asset('css/modalStyle.css')}}">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10 text-center">
			<h1><strong>GESTIÓN DE USUARIOS</strong></h1>
			<a href="#" class="btn btn-t1" data-toggle="modal" data-target="#loginModal"><strong>Agregar</strong></a>
			{{-- Modal Agregar Usuario --}}
			<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="loginModal" style="top: 0 !important; left: 0 !important;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						{{-- Formulario Agregar Usuario --}}
						<div class="modal-header" style="justify-content: center;">
							<h1>REGISTRAR NUEVO USUARIO</h1>
						</div>
						<div class="modal-body">
							<form id="agregarUser" method="POST" action="{{ route('registrar')}}">
								{{ method_field('PUT') }}
								{{ csrf_field() }}
								<div class="row">
									<div class="col-6">
										<label for="NombreCompleto" class="col-md-12 col-form-label text-md-left">Nombre completo<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="NombreCompleto" type="text" class="form-control" name="NombreCompleto" value="{{ old('NombreCompleto') }}" autocomplete="NombreCompleto" autofocus placeholder="Ingrese nombre completo"
												   onkeyup="this.value=CampoLetras(this.value)">
										</div>
									</div>
									<div class="col-6">
										<label for="email" class="col-md-12 col-form-label text-md-left">Correo electrónico<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Ingrese el correo de usuario" autocomplete="email" autofocus>
											@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label for="password" class="col-md-12 col-form-label text-md-left">Contraseña<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Ingrese la contraseña del usuario" autocomplete="password" autofocus>
										</div>
									</div>
									<div class="col-6">
										<label for="password" class="col-md-12 col-form-label text-md-left">Confirmar Contraseña<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Confirme la contraseña" autocomplete="password_confirmation" autofocus>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label for="Rol" class="col-md-12 col-form-label text-md-left">Rol<span class="red">*</span></label>
										<div class="col-md-12">
											<select class="form-control" id="Rol" name="Rol" value="{{ old('Rol') }}" >
												<option value="">Seleccionar</option>
												@foreach($rol as $roles)
												<option value="{{$roles->id_Rol}}">{{$roles->Nombre}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-6">
										<label for="Area" class="col-md-12 col-form-label text-md-left">Área Laboral<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="Area" type="text" class="form-control" name="Area" value="{{ old('Area') }}" placeholder="Ingrese el área del usuario" autocomplete="Area" autofocus>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<label for="TipoDoc" class="col-md-12 col-form-label text-md-left">Tipo de Documento<span class="red">*</span></label>
										<div class="col-md-12">
											<select class="form-control" id="TipoDoc" name="TipoDoc" value="{{ old('TipoDoc') }}">
												<option value="">Seleccionar</option>
												@foreach($tipoDoc as $tipoDocs)
												<option value="{{$tipoDocs->id_Tipo}}">{{$tipoDocs->Nombre}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-6">
										<label for="NumeroDoc" class="col-md-12 col-form-label text-md-left">Número de Documento<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="NumeroDoc" type="text" class="form-control" name="NumeroDoc" value="{{ old('NumeroDoc') }}" placeholder="Ingrese el número de documento" autocomplete="NumeroDoc" autofocus
											onkeypress="return (event.charCode >= 48 && event.charCode <= 57)">
										</div>
									</div>
									<div class="col-6">
										<label for="Celular" class="col-md-12 col-form-label text-md-left">Celular<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="Celular" type="text" class="form-control" name="Celular" value="{{ old('Celular') }}" placeholder="Ingrese el número de celular" autocomplete="Celular" autofocus
											onkeypress="return (event.charCode >= 48 && event.charCode <= 57)">
										</div>
									</div>
								</div>
								<div class="modal-footer" style="margin-top: 15px;">
									<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
									<button type="submit" class="btn btn-env">
										Registrar
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			{{-- fin del modal --}}
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-10 tablestyle table-responsive">
			<table id="listUser" class="table table-light table-striped">
				<thead class="thead-color">
					<tr>
						<th class="text-center th-cl">N°</th>
						<th class="text-center">Área Laboral</th>
						<th class="text-center">Nombres Completo</th>
						<th class="text-center">N° Contacto</th>
						<th class="text-center">Estado</th>
						<th class="text-center">Rol</th>
						<th class="text-center th-cr">Opciones</th>
					</tr>
				</thead>
				<tbody id="listUserBody">
					@foreach($user as $users)
					<tr>
						<td class="text-center">{{$i++}}</td>
						<td class="text-center">{{$users->Area}}</td>
						<td class="text-center">{{$users->NombreCompleto}}</td>
						<td class="text-center">{{$users->Celular}}</td>
						<td class="text-center">{{$users->Estado}}</td>
						@foreach($rol as $roles)
						@if($roles->id_Rol == $users->Rol)
						<td class="text-center">{{$roles->Nombre}}</td>
						@endif
						@endforeach
						<td class="text-center">
							<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditUser{{$users->id}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
							<a href="#" class="btn btn-view" data-toggle="modal" data-target="#ViewUser{{$users->id}}"><img src="{{ asset('img/eye.svg') }}" class="fa-siz"></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@foreach($user as $users)
			{{-- Modal Editar Usuario --}}
			<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="EditUser{{$users->id}}" style="top: 0 !important; left: 0 !important;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						{{-- Formulario Editar Usuario --}}
						<div class="modal-header" style="justify-content: center;">
							<h1>EDITAR INFORMACIÓN DEL USUARIO</h1>
						</div>
						<div class="modal-body">
							@include('usuarios.edit')
						</div>
					</div>
				</div>
			</div>
			{{-- fin del modal --}}

			{{-- Modal Ver detalles Usuario --}}
			<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" id="ViewUser{{$users->id}}" style="top: 0 !important; left: 0 !important;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>VER DETALLES DEL USUARIO</h1>
						</div>
						<div class="modal-body">
							@include('usuarios.show')
						</div>
						<div  class="modal-footer">
							<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
						</div>
					</div>
				</div>
			</div>
			{{-- fin del modal --}}
			@endforeach
		</div>
	</div>
</div>
<script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('js/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('js/minimalist-picker/dobpicker.js')}}"></script>
<script src="{{asset('js/jquery.pwstrength/jquery.pwstrength.js')}}"></script>

<script src="{{asset('js/validaciones/mainAgregarUser.js')}}"></script>

<script>
	$(document).ready(function() {
		$('#listUser').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
@foreach($user as $users)
<script>
	(function($) {
		var form = $(".ediUser{{$users->id}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			NombreCompleto: {
				required: true,
				letterswithbasicpunc: true,
			},
			email: {
				required: true,
				email: true,
			},
			password_new: {
				minlength: 8,
				alphanumeric: true,
			},
			password_confirmation: {
				minlength: 8,
				alphanumeric: true,
				equalTo: "#password_new{{$users->id}}",
			},
			Area: {
				required: true,
			},
			Rol: {
				required: true,
			},
			TipoDoc: {
				required: true,
			},
			NumeroDoc: {
				required: true,
				digits: true,
			},
			Celular: {
				required: true,
				digits: true,
			},
		},
		messages : {
			NombreCompleto: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
				letterswithbasicpunc: 'Solo letras <i class="fas fa-exclamation-triangle"></i>',
			},
			email: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
				email: 'Correo no es valido <i class="fas fa-exclamation-triangle"></i>',
			},
			password_new: {
				minlength: 'La cantraseña debe tener minimo 8 caracateres <i class="fas fa-exclamation-triangle"></i>',
				alphanumeric: 'Solo se permiten números y letras <i class="fas fa-exclamation-triangle"></i>',
			},
			password_confirmation: {
				minlength: 'La cantraseña debe tener minimo 8 caracateres <i class="fas fa-exclamation-triangle"></i>',
				alphanumeric: 'Solo se permiten números y letras <i class="fas fa-exclamation-triangle"></i>',
				equalTo: 'Las contraseñas deben coincidir <i class="fas fa-exclamation-triangle"></i>',
			},
			Area: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Rol: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			TipoDoc: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			NumeroDoc: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
				digits: 'Solo se permiten números enteros <i class="fas fa-exclamation-triangle"></i>'
			},
			Celular: {
				required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
				digits: 'Solo se permiten números enteros <i class="fas fa-exclamation-triangle"></i>'
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});

	})(jQuery);
	   
</script>
<script>
function CampoLetras(string) { 
    var out = '';
    //Se añaden las letras validas
    var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ '; //Caracteres validos

    for (var i = 0; i < string.length; i++)
        if (filtro.indexOf(string.charAt(i)) != -1)
            out += string.charAt(i);
    return out;
}
</script>
	
@endforeach
@endsection