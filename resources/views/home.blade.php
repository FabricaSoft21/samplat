@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/datatable.css')}}">
<link rel="stylesheet" href="{{asset('css/modalStyle.css')}}">
<link rel="stylesheet" href="{{asset('css/cardStyle.css')}}">
<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<link rel="stylesheet" href="{{asset('css/file-input.css') }}">
<body>
    <div class="container">
        {{-- <div class="row" style="padding-right: 11%">
            <div class="col-12">
                <div class="col-md-3 float-right" style="display: flex;">
                    <label class="filt"><b>Filtrar:</b></label>
                    <select name="Prioridad" id="Prioridad" class="form-control">
                        <option value="">Prioridad</option>
                        <option value="Baja">Baja</option>
                        <option value="Media">Media</option>
                        <option value="Alta">Alta</option>
                    </select>
                </div>
            </div>
        </div> --}}
        @if(Auth::user()->Rol == 1)
        <div class="row">
            {{-- Prioridad Alta --}}
            @foreach($solMant as $solMants)
            @if($solMants->Estado === "Abierto")
            @if($solMants->Prioridad == 1)
            <div class="card" style="width: 18rem;">
                <h5 class="card-title ti-a"><b id="clock{{$solMants->id_Solicitud}}"></b></h5>
                <div class="card-img-top" >
                    @foreach($img as $imgs)
                    @if($solMants->id_Solicitud === $imgs->id_Imagen)
                    <a @if($imgs->Imagen_1 != null) href="#" data-toggle="modal" data-target="#ImgSlide{{$solMants->id_Solicitud}} @endif"><img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAMlogo.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="Logo" class="img-size"></a>
                    @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <p class="card-text">{{$solMants->Descripcion}}</p>
                </div>
                <div class="card-body text-center">
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListTecnicos{{$solMants->id_Solicitud}}">Asignar técnico</a>
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListDetalle{{$solMants->id_Solicitud}}">Ver detalles</a>
                </div>
            </div>
            @endif

            {{-- Prioridad Media --}}
            @if($solMants->Prioridad == 2)
            <div class="card" style="width: 18rem;">
                <h5 class="card-title ti-m"><b id="clock{{$solMants->id_Solicitud}}">{{-- {{$solMants->Fecha_solicitud}} --}}</b></h5>
                <div class="card-img-top" >
                    @foreach($img as $imgs)
                    @if($solMants->id_Solicitud === $imgs->id_Imagen)
                    <a @if($imgs->Imagen_1 != null) href="#" data-toggle="modal" data-target="#ImgSlide{{$solMants->id_Solicitud}} @endif"> <@if($imgs->Imagen_1 == null) <img src="{{ asset('img/SAM1000X1000.png') }}" alt="Logo" class="img-size"> @else <img src="../../solicitudImg/{{$imgs->Imagen_1}}" alt="Logo" class="img-size"> @endif></a>
                    @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <p class="card-text">{{$solMants->Descripcion}}</p>
                </div>
                <div class="card-body text-center">
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListTecnicos{{$solMants->id_Solicitud}}">Asignar técnico</a>
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListDetalle{{$solMants->id_Solicitud}}">Ver detalles</a>
                </div>
            </div>
            @endif

            {{-- Prioridad Baja --}}
            @if($solMants->Prioridad == 3)
            <div class="card" style="width: 18rem;">
                <h5 class="card-title ti-b"><b id="clock{{$solMants->id_Solicitud}}"></b></h5>
                <div class="card-img-top" >
                    @foreach($img as $imgs)
                    @if($solMants->id_Solicitud === $imgs->id_Imagen)
                    <a @if($imgs->Imagen_1 != null) href="#" data-toggle="modal" data-target="#ImgSlide{{$solMants->id_Solicitud}} @endif"><img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="Logo" class="img-size"></a>
                    @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <p class="card-text">{{$solMants->Descripcion}}</p>
                </div>
                <div class="card-body text-center">
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListTecnicos{{$solMants->id_Solicitud}}">Asignar técnico</a>
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListDetalle{{$solMants->id_Solicitud}}">Ver detalles</a>
                </div>
            </div>
            @endif
            @endif

            {{-- Modal Slider --}}
            <div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="SlideModalLabel" id="ImgSlide{{$solMants->id_Solicitud}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="justify-content: center;">
                            <h1>VER IMAGENES</h1>
                        </div>
                        <div class="modal-body">
                            <div id="carouselExampleControls{{$solMants->id_Solicitud}}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="First slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_2 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_2}}" @endif alt="Second slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_3 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_3}}" @endif alt="Third slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_4 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_4}}" @endif alt="Four slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_5 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_5}}" @endif alt="Five slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls{{$solMants->id_Solicitud}}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls{{$solMants->id_Solicitud}}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Fin Slider --}}

            {{-- Modal Listar Tecnicos --}}
            <div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="TecniModalLabel" id="ListTecnicos{{$solMants->id_Solicitud}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="justify-content: center;">
                            <h1>LISTA DE TÉCNICOS</h1>
                        </div>
                        <div class="modal-body">
                            <table id="ListTec" class="table table-light table-striped ListTec">
                                <thead class="thead-color">
                                    <tr>
                                        <th class="text-center th-cl">Nombre Completo</th>
                                        <th class="text-center">Área Laboral</th>
                                        <th class="text-center th-cr">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user as $users)
                                    @if($users->Rol === 2)
                                    <tr>
                                        <td class="text-center">{{$users->NombreCompleto}}</td>
                                        <td class="text-center">{{$users->Area}}</td>
                                        <td class="text-center">
                                            <form method="POST" action="{{route('asigTec',[$solMants->id_Solicitud])}}">
                                                {{ csrf_field() }}
                                                @method('PUT')
                                                <input type="hidden" name="Estado" value="Asignado">
                                                <input type="hidden" name="Tecnico" value="{{$users->id}}">
                                                <input type="hidden" name="EmailUser" value="{{$solMants->Email}}">
                                                <input type="hidden" name="NombreTec" value="{{$users->NombreCompleto}}">
                                                <button type="submit" class="btn btn-edit"><img src="{{ asset('img/datos-verificados.svg') }}" class="fa-siz"></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer" style="margin-top: 15px;">
                            <a data-dismiss="modal" class="btn btn-close">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Fin Modal --}}
            @endforeach
        </div>
        @endif
        <div class="row">
            @foreach($solMant as $solMants)
            @if(Auth::user()->Rol == 2 && Auth::user()->id === $solMants->Tecnico)
            @if($solMants->Estado === "Asignado")

            {{-- Prioridad Alta --}}
            @if($solMants->Prioridad === 1)
            <div class="card" style="width: 18rem;">
                <h5 class="card-title ti-a"><b id="clock{{$solMants->id_Solicitud}}"></b></h5>
                <div class="card-img-top" >
                    @foreach($img as $imgs)
                    @if($solMants->id_Solicitud === $imgs->id_Imagen)
                    <a @if($imgs->Imagen_1 != null) href="#" data-toggle="modal" data-target="#ImgSlide{{$solMants->id_Solicitud}} @endif"><img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="Logo" class="img-size"></a>
                    @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <p class="card-text">{{$solMants->Descripcion}}</p>
                </div>
                <div class="card-body text-center">
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListDetalle{{$solMants->id_Solicitud}}">Ver detalles</a>
                    @if($solMants->InsumoAsignado === 0)
                    <a href="#" class="btn btn-f2" data-toggle="modal" data-target="#Insumos{{$solMants->id_Solicitud}}">Asignar insumos</a>
                    @else
                    <a href="#" class="btn btn-f2" data-toggle="modal" data-target="#CerrarCaso{{$solMants->id_Solicitud}}">Cerrar caso</a>
                    @endif
                </div>
            </div>
            @endif
            {{-- Fin modal --}}

            {{-- Prioridad Media --}}
            @if($solMants->Prioridad === 2)
            <div class="card" style="width: 18rem;">
                <h5 class="card-title ti-m"><b id="clock{{$solMants->id_Solicitud}}"></b></h5>
                <div class="card-img-top" >
                    @foreach($img as $imgs)
                    @if($solMants->id_Solicitud === $imgs->id_Imagen)
                    <a @if($imgs->Imagen_1 != null) href="#" data-toggle="modal" data-target="#ImgSlide{{$solMants->id_Solicitud}} @endif"><img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="Logo" class="img-size"></a>
                    @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <p class="card-text">{{$solMants->Descripcion}}</p>
                </div>
                <div class="card-body text-center">
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListDetalle{{$solMants->id_Solicitud}}">Ver detalles</a>
                    @if($solMants->InsumoAsignado === 0)
                    <a href="#" class="btn btn-f2" data-toggle="modal" data-target="#Insumos{{$solMants->id_Solicitud}}">Asignar insumos</a>
                    @else
                    <a href="#" class="btn btn-f2" data-toggle="modal" data-target="#CerrarCaso{{$solMants->id_Solicitud}}">Cerrar caso</a>
                    @endif
                </div>
            </div>
            @endif
            {{-- Fin modal --}}

            {{-- Prioridad Baja --}}
            @if($solMants->Prioridad === 3)
            <div class="card" style="width: 18rem;">
                <h5 class="card-title ti-b"><b id="clock{{$solMants->id_Solicitud}}"></b></h5>
                <div class="card-img-top" >
                    @foreach($img as $imgs)
                    @if($solMants->id_Solicitud === $imgs->id_Imagen)
                    <a @if($imgs->Imagen_1 != null) href="#" data-toggle="modal" data-target="#ImgSlide{{$solMants->id_Solicitud}} @endif"><img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="Logo" class="img-size"></a>
                    @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <p class="card-text">{{$solMants->Descripcion}}</p>
                </div>
                <div class="card-body text-center">
                    <a href="#" class="btn btn-f1" data-toggle="modal" data-target="#ListDetalle{{$solMants->id_Solicitud}}">Ver detalles</a>
                    @if($solMants->InsumoAsignado === 0)
                    <a href="#" class="btn btn-f2" data-toggle="modal" data-target="#Insumos{{$solMants->id_Solicitud}}">Asignar insumos</a>
                    @else
                    <a href="#" class="btn btn-f2" data-toggle="modal" data-target="#CerrarCaso{{$solMants->id_Solicitud}}">Cerrar caso</a>
                    @endif
                </div>
            </div>
            @endif
            {{-- Fin modal --}}

            @endif
            @endif

            {{-- Modal Slider --}}
            <div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="SlideModalLabel" id="ImgSlide{{$solMants->id_Solicitud}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="justify-content: center;">
                            <h1>VER IMAGENES</h1>
                        </div>
                        <div class="modal-body">
                            <div id="carouselExampleControls{{$solMants->id_Solicitud}}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_1}}" @endif alt="First slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_2 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_2}}" @endif alt="Second slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_3 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_3}}" @endif alt="Third slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_4 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_4}}" @endif alt="Four slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                    <div class="carousel-item">
                                        @foreach($img as $imgs)
                                        @if($solMants->id_Solicitud === $imgs->id_Imagen)
                                        <img @if($imgs->Imagen_5 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../solicitudImg/{{$imgs->Imagen_5}}" @endif alt="Five slide" class="img-slid">
                                        @endif
                                        @endforeach
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls{{$solMants->id_Solicitud}}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls{{$solMants->id_Solicitud}}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Fin Slider --}}

            {{-- Modal Detalles Solicitud --}}
            <div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="TecniModal" id="ListDetalle{{$solMants->id_Solicitud}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="justify-content: center;">
                            <h1>DETALLE DE LA SOLICITUD</h1>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <h4>Nombre del Solicitante</h4>
                                    <input type="text" class="form-control" name="NombreCompleto" id="NombreCompleto" value="{{$solMants->NombreCompleto}}" disabled="">
                                </div>
                                <div class="col-6">
                                    <h4>Correo Electronico del Solicitante</h4>
                                    <input type="email" class="form-control" name="Email" id="Email" value="{{$solMants->Email}}" disabled=""><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <h4>Categoria de la Solicitud</h4>
                                    @foreach($categoria as $cate)
                                    @if($cate->id_Categoria === $solMants->Categoria)
                                    <input type="text" class="form-control" name="NombreCompleto" id="NombreCompleto" value="{{$cate->Nombre}}" disabled="">
                                    @endif
                                    @endforeach
                                </div>
                                <div class="col-6">
                                    <h4>Técnico Asignado</h4>
                                    <input type="text" class="form-control" name="Tecnico" id="Tecnico"@foreach($user as $users) @if($users->id === $solMants->Tecnico) value="{{$users->NombreCompleto}}"@endif @endforeach placeholder="No hay técnico asignado" disabled=""><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <h4>Estado</h4>
                                    <input type="text" class="form-control" name="Estado" id="Estado" value="{{$solMants->Estado}}" disabled="">
                                </div>
                                <div class="col-6">
                                    <h4>Prioridad</h4>
                                    @foreach($prioridad as $priori)
                                    @if($priori->id_Prioridad === $solMants->Prioridad)
                                    <input type="text" class="form-control" name="Prioridad" id="Prioridad" value="{{$priori->Prioridad}}" disabled=""><br>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <h4>Ubicacion del Caso</h4>
                                    @foreach($edificio as $edificios)
                                    @foreach($piso as $pisos)
                                    @foreach($espacio as $espacios)
                                    @if($solMants->Edificio === $edificios->id_Edificio)
                                    @if($solMants->Piso === $pisos->id_Piso)
                                    @if($solMants->Espacio === $espacios->id_Espacio)
                                    <input type="text" class="form-control" name="NombreCompleto" id="NombreCompleto" value="{{$edificios->Nombre}} - {{$pisos->Piso}} - {{$espacios->Espacio}}" disabled="">
                                    @endif
                                    @endif
                                    @endif
                                    @endforeach
                                    @endforeach
                                    @endforeach
                                </div>
                                <div class="col-6">
                                    <h4>Fecha de solicitud del Caso</h4>
                                    <input type="text" class="form-control" name="Fecha_solicitud" id="Fecha_solicitud" value="{{$solMants->Fecha_solicitud}}" disabled=""><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h4>Descripcion del caso</h4>
                                    <textarea class="form-control" disabled="" rows="3">{{$solMants->Descripcion}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="margin-top: 15px;">
                            <a data-dismiss="modal" class="btn btn-close">Cerrar</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Fin Modal --}}

            {{-- Modal Registrar Insumos --}}
            <div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="insumoModalLabel" id="Insumos{{$solMants->id_Solicitud}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        {{-- formulario Insumo --}}
                        <div class="modal-header" style="justify-content: center;">
                            <h1>ASIGANAR INSUMOS AL CASO</h1>
                        </div>
                        <div class="modal-body">
                            <form class="" id="asignarInsumo{{$solMants->id_Solicitud}}" method="POST" action="{{route('solicInsumo',[$solMants->id_Solicitud])}}">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}
                                <div class="row field_wrapper">
                                    <div class="col-5">
                                        <label for="Insumo" class="col-md-12 form-label text-md-left">Insumo<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <input type="hidden" name="id_Solicitud" value="{{$solMants->id_Solicitud}}">
                                            <select class="form-control" id="Insumos" name="Insumo[]" value="{{ old('Insumo') }}" >
                                                <option value="">Seleccionar</option>
                                                @foreach($insumo as $insumos)
                                                <option value="{{$insumos->id_Insumo}}">{{$insumos->Insumo}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <label for="Cantidad" class="col-md-12 form-label text-md-left">Cantidad<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <input id="Cantidad" type="number" class="form-control" name="Cantidad[]" value="{{ old('Cantidad') }}" autocomplete="Cantidad" autofocus placeholder="Ingrese la cantidad de insumos">
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="col add_button" title="Add field"><img class="imgAdd" src="{{ asset('img/interface.svg')}}"/></a>
                                </div>
                                <div class="modal-footer" style="margin-top: 15px;">
                                    <a data-dismiss="modal" class="btn btn-close">Cerrar</a>
                                    <button type="submit" class="btn btn-env">
                                        Registrar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Fin Modal --}}

            {{-- Modal Cerrar Caso --}}
            <div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="TecniModalLabel" id="CerrarCaso{{$solMants->id_Solicitud}}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header" style="justify-content: center;">
                            <h1>FICHA DEL MANTENIMIENTO</h1>
                        </div>
                        <div class="modal-body">
                            {{-- Formulario Cerrar Caso --}}
                            <form id="fichaMantes{{$solMants->id_Solicitud}}" class="" method="POST" enctype="multipart/form-data" action="{{route('fichaMante',[$solMants->id_Solicitud])}}">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-5">
                                        <input type="hidden" name="id_Solicitud" value="{{$solMants->id_Solicitud}}">
                                        <label for="TipoMante" class="col-md-12 form-label text-md-left">Tipo de mantenimiento<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <select class="form-control" id="TipoMante" name="TipoMante" value="{{ old('TipoMante') }}" style="display: initial;">
                                                <option value="">Seleccionar</option>
                                                <option value="Preventivo">Preventivo</option>
                                                <option value="Correctivo">Correctivo</option>
                                                <option value="Predictivo">Predictivo</option>
                                            </select><br>
                                        </div>
                                        <label for="EstadoSoli" class="col-md-12 form-label text-md-left Mtop">Estado de la solicitud<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <select class="form-control" id="EstadoSoli" name="EstadoSoli" value="{{ old('EstadoSoli') }}" style="display: initial;">
                                                <option value="">Seleccionar</option>
                                                <option value="Pendiente">Pendiente</option>
                                                <option value="En proceso">En proceso</option>
                                                <option value="En verificación">En verificación</option>
                                            </select><br>
                                        </div>
                                        <label for="InsumosUtil" class="col-md-12 form-label text-md-left Mtop">Insumos utilizados<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <textarea type="text" class="form-control" name="InsumosUtil" id="InsumosUtil" rows="8" disabled="">
                                                @foreach($insumoSol as $insumoSols)
                                                @foreach($insumo as $insumos)
                                                @if($insumoSols->id_Solicitud === $solMants->id_Solicitud && $insumos->id_Insumo === $insumoSols->id_Insumo)
                                                {{$insumos->Insumo}} --> {{$insumoSols->CantidadUsada}}
                                                @endif
                                                @endforeach
                                                @endforeach
                                            </textarea><br>
                                        </div>
                                        <label for="ImagenSoli" class="col-md-12 form-label text-md-left">Adjuntar imagenes de la solicitud<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <input type="file" name="ImagenSoli[]" id="file-2{{$solMants->id_Solicitud}}" class="inputfile inputfile-2" data-multiple-caption="{count} Imagenes seleccionadas" multiple accept="image/*" />
                                            <label for="file-2{{$solMants->id_Solicitud}}">
                                                <span class="iborrainputfile">Adjuntar imagen</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <label for="Observacion" class="col-md-12 form-label text-md-left">Observaciones<span class="red">*</span></label>
                                        <div class="col-md-12">
                                            <textarea type="text" class="form-control" name="Observacion" id="Observacion" rows="6" style="display: initial;"></textarea><br>
                                        </div>
                                        <label for="insumoSob" class="col-md-12 form-label text-md-left">Insumos sobrantes<span class="red">*</span></label>
                                        @foreach($insumoSol as $insumoSols)
                                        @foreach($insumo as $insumos)
                                        @if($insumoSols->id_Solicitud === $solMants->id_Solicitud && $insumos->id_Insumo === $insumoSols->id_Insumo)
                                        <div class="col-md-12">
                                            <select class="form-control col-12 float-left Mbott" id="insumoSob{{$insumos->id_Insumo}}{{$solMants->id_Solicitud}}" name="insumoSob[]" value="{{ old('insumoSob') }}" >
                                                <option value="{{$insumos->id_Insumo}}">{{$insumos->Insumo}}</option>
                                            </select>
                                            <input id="cantidadSob{{$insumos->id_Insumo}}{{$solMants->id_Solicitud}}" name="cantidadSob[] {{$insumos->id_Insumo}}" type="number" class="form-control Mbott"  value="0" min="0" max="{{$insumoSols->CantidadUsada}}" required="">
                                        </div>
                                        @endif
                                        @endforeach
                                        @endforeach
                                    </div>
                                </div>
                                <div class="modal-footer" style="margin-top: 15px;">
                                    <a data-dismiss="modal" class="btn btn-close">Cerrar</a>
                                    <button type="submit" class="btn btn-env">
                                        Registrar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Fin Modal --}}
            @endforeach
        </div>
    </div>
</body>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('js/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('js/minimalist-picker/dobpicker.js')}}"></script>
<script src="{{asset('js/jquery.pwstrength/jquery.pwstrength.js')}}"></script>

<script src="{{asset('js/custom-file-input.js')}}"></script>

<script>
    $(document).ready(function() {
        $('.ListTec').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
            }
        });
    });
</script>

@include ('sweet::alert')

<script>
    const getRemainingTime = deadline => {
      let now = new Date(),
      remainTime = (new Date(deadline) - now + 1000) / 1000,
      remainSeconds = ('0' + Math.floor(remainTime % 60)).slice(-2),
      remainMinutes = ('0' + Math.floor(remainTime / 60 % 60)).slice(-2),
      remainHours = ('0' + Math.floor(remainTime / 3600 % 744)).slice(-3),
      remainDays = Math.floor(remainTime / (3600 * 24));

      return {
        remainSeconds,
        remainMinutes,
        remainHours,
        remainDays,
        remainTime
    }
};

const countdown = (deadline,elem,finalMessage) => {
  const el = document.getElementById(elem);

  const timerUpdate = setInterval( () => {
    let t = getRemainingTime(deadline);
    el.innerHTML = `${t.remainHours}h:${t.remainMinutes}m:${t.remainSeconds}s`;

    if(t.remainTime <= 1) {
      clearInterval(timerUpdate);
      el.innerHTML = finalMessage;
  }

}, 1000)
};
@foreach($solMant as $solMants)
countdown('{{$solMants->Fecha_fin}}', 'clock{{$solMants->id_Solicitud}}', '¡Este caso esta retrasado!');
@endforeach
</script>

<script>
    $(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper

    var x = 1; //Initial field counter is 1
    var z = 1;
    var y = 1;
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            z++;
            y++;
            $(wrapper).append('<div><div class="col-5 float-left Mtop"><div class="col-md-12"><select class="form-control" id="Insumo'+z+'" name="Insumo[]'+z+'" value="{{ old('Insumo') }}" required><option value="">Seleccionar</option>@foreach($insumo as $insumos)<option value="{{$insumos->id_Insumo}}">{{$insumos->Insumo}}</option>@endforeach</select></div></div><div class="col-5 float-left Mtop"><div class="col-md-12"><input id="Cantidad'+y+'" type="number" class="form-control" name="Cantidad[]'+y+'" value="{{old('Cantidad')}}" autocomplete="Cantidad" autofocus placeholder="Ingrese la cantidad de insumos" required min="1"></div></div><a href="javascript:void(0);" class="remove_button" title="Remove field"><img class="imgRem" src="{{ asset('img/signs.svg')}}"/></a></div>'); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

@foreach($solMant as $solMants)
<script>
    (function($) {
        var form = $("#fichaMantes{{$solMants->id_Solicitud}}");
        form.validate({errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        },
        rules: {
            TipoMante: {
                required: true,
            },
            EstadoSoli: {
                required: true,
            },
            Observacion: {
                required: true,
            },
        },
        messages : {
            TipoMante: {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            EstadoSoli: {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            Observacion: {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            @foreach($insumo as $insumos)
            'cantidadSob[] {{$insumos->id_Insumo}}': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor o igual a 0 <i class="fas fa-exclamation-triangle"></i>',
                max: 'La cantidad excede la utilizada <i class="fas fa-exclamation-triangle"></i>',
            },
            @endforeach
        },
        onfocusout: function(element) {
            $(element).valid();
        },
    });

    })(jQuery);
</script>

<script>
    (function($) {
        var form = $("#asignarInsumo{{$solMants->id_Solicitud}}");
        form.validate({errorPlacement: function errorPlacement(error, element) {
            element.before(error);
        },
        rules: {
            'Insumo[]': {
                required: true,
            },

            'Cantidad[]': {
                required: true,
                min: 1,
            },
        },
        messages : {
            'Insumo[]': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]2': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]3': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]4': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]5': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]6': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]7': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]8': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]9': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Insumo[]10': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]2': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]3': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]4': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]5': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]6': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]7': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]8': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]9': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
            'Cantidad[]10': {
                required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
                number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
                min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            },
        },
        onfocusout: function(element) {
            $(element).valid();
        },
    });

})(jQuery);
</script>
@endforeach
@endsection