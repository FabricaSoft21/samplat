@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<link rel="stylesheet" href="{{asset('css/datatable.css')}}">
<link rel="stylesheet" href="{{asset('css/modalStyle.css')}}">

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10 text-center">
			<div class="col-md-10 text-center">
				<h1><strong>GESTIÓN DE EQUIPOS</strong></h1>
				<a href="#" class="btn btn-t4" data-toggle="modal" data-target="#EquipoModal"><strong>Agregar Equipo</strong></a>
				<a href="#" class="btn btn-t4" data-toggle="modal" data-target="#MarcaModal"><strong>Agregar Marca</strong></a>
				<a href="#" class="btn btn-t4" data-toggle="modal" data-target="#TipoModal"><strong>Agregar Tipo</strong></a>
				{{-- Modal Agregar Equipo --}}
				<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="equipoModalLabel" id="EquipoModal" style="top: 0 !important; left: 0 !important;">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							{{-- Formulario Agregar Equipo --}}
							<div class="modal-header" style="justify-content: center;">
								<h1>REGISTRAR NUEVO EQUIPO</h1>
							</div>
							<div class="modal-body">
								<form id="agregarEquipo" method="POST" action="{{ route('storeEquipo')}}">
									{{ method_field('PUT') }}
									{{ csrf_field() }}
									<div class="row">
										<div class="col-6">
											<label for="Categoria" class="col-md-12 col-form-label text-md-left">Categoria<span class="red">*</span></label>
											<div class="col-md-12">
												<select class="form-control" id="Categoria" name="Categoria" value="{{ old('Categoria') }}" >
													<option value="">Seleccionar</option>
													@foreach($categorias as $cate)
													<option value="{{$cate->id_Categoria}}">{{$cate->Nombre}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-6">
											<label for="Codigo" class="col-md-12 col-form-label text-md-left">Codigo del equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Codigo" type="text" class="form-control" name="Codigo" value="{{ old('Codigo') }}" autocomplete="Codigo" autofocus placeholder="Ingrese el codigo del equipo">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label for="Modelo" class="col-md-12 col-form-label text-md-left">Modelo del equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Modelo" type="text" class="form-control" name="Modelo" value="{{ old('Modelo') }}" placeholder="Ingrese el modelo del equipo" autocomplete="Modelo" autofocus>
											</div>
										</div>
										<div class="col-6">
											<label for="Serie" class="col-md-12 col-form-label text-md-left">Serie del equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Serie" type="text" class="form-control" name="Serie" value="{{ old('Serie') }}" placeholder="Ingrese la serie del equipo" autocomplete="Serie" autofocus>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label for="Marca" class="col-md-12 col-form-label text-md-left">Marca del equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<select class="form-control" id="Marca" name="Marca" value="{{ old('Marca') }}" >
													<option value="">Seleccionar</option>
													@foreach($marca as $marcas)
													<option value="{{$marcas->id_Marca}}">{{$marcas->Marca}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col-6">
											<label for="Tipo" class="col-md-12 col-form-label text-md-left">Tipo de equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<select class="form-control" id="Tipo" name="Tipo" value="{{ old('Tipo') }}" >
													<option value="">Seleccionar</option>
													@foreach($tipo as $tipos)
													<option value="{{$tipos->id_Tipo}}">{{$tipos->Tipo}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label for="Dependencia" class="col-md-12 col-form-label text-md-left">Dependencia del equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Dependencia" type="text" class="form-control" name="Dependencia" value="{{ old('Dependencia') }}" placeholder="Ingrese la dependencia del equipo" autocomplete="Dependencia" autofocus>
											</div>
										</div>
										<div class="col-6">
											<label for="PSB" class="col-md-12 col-form-label text-md-left">PSB<span class="red">*</span></label>
											<div class="form-check col-6 float-left">
												<div class="form-control">
													<input class="form-check-input" type="radio" name="PSB" id="PSB" value="1" checked>
													<label class="form-check-label" for="PSB">
														Si
													</label>
												</div>
											</div>
											<div class="form-check col-6 float-right">
												<div class="form-control">
													<input class="form-check-input" type="radio" name="PSB" id="PSB" value="0">
													<label class="form-check-label" for="PSB">
														No
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-6">
											<label for="Placa" class="col-md-12 col-form-label text-md-left">Placa del equipo<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Placa" type="text" class="form-control" name="Placa" value="{{ old('Placa') }}" placeholder="Ingrese la placa de inventario del equipo" autocomplete="Placa" autofocus>
											</div>
										</div>
									</div>
									<div class="modal-footer" style="margin-top: 15px;">
										<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
										<button type="submit" class="btn btn-env">
											Registrar
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				{{-- fin del modal --}}
				{{-- Modal Agregar Marca --}}
				<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="marcaModalLabel" id="MarcaModal" style="top: 0 !important; left: 0 !important;">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							{{-- Formulario Agregar Marca --}}
							<div class="modal-header" style="justify-content: center;">
								<h1>REGISTRAR NUEVA MARCA</h1>
							</div>
							<div class="modal-body">
								<form id="agregarMarca" method="POST" action="{{ route('storeMarca')}}">
									{{ method_field('PUT') }}
									{{ csrf_field() }}
									<div class="row justify-content-center">
										<div class="col-8">
											<label for="Marca" class="col-md-12 col-form-label text-md-left">Nombre marca<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Marca" type="text" class="form-control" name="Marca" value="{{ old('Marca') }}" autocomplete="Marca" autofocus placeholder="Ingrese la marca">
											</div>
										</div>
									</div>
									<div class="modal-footer" style="margin-top: 15px;">
										<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
										<button type="submit" class="btn btn-env">
											Registrar
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				{{-- fin del modal --}}
				{{-- Modal Agregar Tipo --}}
				<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="topoModalLabel" id="TipoModal" style="top: 0 !important; left: 0 !important;">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							{{-- Formulario Agregar Tipo --}}
							<div class="modal-header" style="justify-content: center;">
								<h1>REGISTRAR NUEVO TIPO</h1>
							</div>
							<div class="modal-body">
								<form id="agregarTipo" method="POST" action="{{ route('storeTipo')}}">
									{{ method_field('PUT') }}
									{{ csrf_field() }}
									<div class="row justify-content-center">
										<div class="col-8">
											<label for="Tipo" class="col-md-12 col-form-label text-md-left">Nombre tipo<span class="red">*</span></label>
											<div class="col-md-12">
												<input id="Tipo" type="text" class="form-control" name="Tipo" value="{{ old('Tipo') }}" autocomplete="Tipo" autofocus placeholder="Ingrese el tipo">
											</div>
										</div>
									</div>
									<div class="modal-footer" style="margin-top: 15px;">
										<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
										<button type="submit" class="btn btn-env">
											Registrar
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				{{-- fin del modal --}}
			</div>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-10 tablestyle table-responsive">
			<table id="listUser" class="table table-light table-striped">
				<thead class="thead-color">
					<tr>
						<th class="text-center th-cl">N°</th>
						<th class="text-center">Codigo</th>
						<th class="text-center">Modelo</th>
						<th class="text-center">Serie</th>
						<th class="text-center">Placa</th>
						<th class="text-center th-cr">Opciones</th>
					</tr>
				</thead>
				<tbody id="listEquipoBody">
					@foreach($equipo as $equipos)
					<tr>
						<td class="text-center">{{$i++}}</td>
						<td class="text-center">{{$equipos->Codigo}}</td>
						<td class="text-center">{{$equipos->Modelo}}</td>
						<td class="text-center">{{$equipos->Serie}}</td>
						<td class="text-center">{{$equipos->Placa}}</td>
						<td class="text-center">
							<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#EditEquipo{{$equipos->id_Equipo}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
							<a href="#" class="btn btn-view" data-toggle="modal" data-target="#ViewEquipo{{$equipos->id_Equipo}}"><img src="{{ asset('img/eye.svg') }}" class="fa-siz"></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@foreach($equipo as $equipos)
			{{-- Modal Editar Equipos --}}
			<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="equipoModalLabel" id="EditEquipo{{$equipos->id_Equipo}}" style="top: 0 !important; left: 0 !important;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						{{-- Formulario Editar Equipo --}}
						<div class="modal-header" style="justify-content: center;">
							<h1>EDITAR EQUIPO</h1>
						</div>
						<div class="modal-body">
							@include('equipos.edit')
						</div>
					</div>
				</div>
			</div>
			{{-- fin del modal --}}

			{{-- Modal Ver Detalles Equipo --}}
			<div class="modal fade col-12" tabindex="-1" role="dialog" aria-labelledby="equipoModalLabel" id="ViewEquipo{{$equipos->id_Equipo}}" style="top: 0 !important; left: 0 !important;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>VER DETALLES DEL EQUIPO</h1>
						</div>
						<div class="modal-body">
							@include('equipos.show')
						</div>
						<div  class="modal-footer">
							<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
						</div>
					</div>
				</div>
			</div>
			{{-- fin del modal --}}
			@endforeach
		</div>
	</div>
</div>

<script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('js/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('js/minimalist-picker/dobpicker.js')}}"></script>
<script src="{{asset('js/jquery.pwstrength/jquery.pwstrength.js')}}"></script>

<script src="{{asset('js/validaciones/mainEquipo.js')}}"></script>

<script>
	$(document).ready(function() {
		$('#listUser').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>
@foreach($equipo as $equipos)
<script>
	(function($) {
		var form = $(".editarEquipo{{$equipos->id_Equipo}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			Categoria: {
				required: true,
			},
			Codigo: {
				required: true,
			},
			Modelo: {
				required: true,
			},
			Serie: {
				required: true,
			},
			Marca: {
				required: true,
			},
			Tipo: {
				required: true,
			},
			Cantidad: {
				required: true,
				number: true,
				min: 1,
			},
			Dependencia: {
				required: true,
			},
			Placa: {
				required: true,
			},
			MarcaA:{
				required: true,
			},
		},
		messages : {
			Categoria: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Codigo: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Modelo: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Serie: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Marca: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Tipo: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Cantidad: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Solo números <i class="fas fa-exclamation-triangle"></i>',
				min: 'La cantidad debe ser mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			Dependencia: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Placa: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			MarcaA: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});
	})(jQuery);
</script>
@endforeach
@endsection