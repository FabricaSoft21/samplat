(function($) {
    var form = $("#agregarEquipo");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Categoria: {
            required: true,
        },
        Codigo: {
            required: true,
        },
        Modelo: {
            required: true,
        },
        Serie: {
            required: true,
        },
        Marca: {
            required: true,
        },
        Tipo: {
            required: true,
        },
        Cantidad: {
            required: true,
            number: true,
            min: 1,
        },
        Dependencia: {
            required: true,
        },
        Placa: {
            required: true,
        },
        MarcaA:{
            required: true,
        },
    },
    messages : {
        Categoria: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Codigo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Modelo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Serie: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Marca: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Tipo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Cantidad: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            number: 'Solo números <i class="fas fa-exclamation-triangle"></i>',
            min: 'La cantidad debe ser mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
        },
        Dependencia: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Placa: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        MarcaA: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
})(jQuery);

(function($) {
    var form = $("#agregarMarca");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Marca:{
            required: true,
        },
    },
    messages : {
        Marca: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
})(jQuery);

(function($) {
    var form = $("#agregarTipo");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Tipo:{
            required: true,
        },
    },
    messages : {
        Tipo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
})(jQuery);