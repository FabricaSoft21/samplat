(function($) {
    var form = $("#SolicitudForm");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Categoria: {
            required: true,
        },
        Edificio: {
            required: true,
        },
        Espacio: {
            required: true,
        },
        Piso: {
            required: true,
        },
        NombreCompleto: {
            required: true,
            letterswithbasicpunc: true,
        },
        Email: {
            required: true,
            email: true,
        },
        Prioridad: {
            required: true,
        },
        Descripcion: {
            required: true,
            maxlength: 210,
        },
    },
    messages : {
        Categoria: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Edificio: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Espacio: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Piso: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        NombreCompleto: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            letterswithbasicpunc: 'Solo letras <i class="fas fa-exclamation-triangle"></i>',
        },
        Email: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            email: 'El correo no es valido <i class="fas fa-exclamation-triangle"></i>',
        },
        Prioridad: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Descripcion: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            maxlength: 'La descripcion es demaciado larga <i class="fas fa-exclamation-triangle"></i>'
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
}) (jQuery);