(function($) {
    var form = $("#pisoValidate");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Piso: {
            required: true,
        },
    },
    messages : {
        Piso: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
})(jQuery);