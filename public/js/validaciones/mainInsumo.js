(function($) {
    var form = $("#insumoValidate");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Insumo: {
            required: true,
        },
        Cantidad: {
            required: true,
            number: true,
            min: 1,
        },
    },
    messages : {
        Insumo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Cantidad: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            number: 'Solo números enteros <i class="fas fa-exclamation-triangle"></i>',
            min: 'La cantidad debe ser mayor o igual a 1 <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
})(jQuery);