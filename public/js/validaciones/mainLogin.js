(function($) {
    var form = $("#login");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        email: {
            required: true,
            email: true,
        },
        password: {
            required: true,
        },
    },
    messages : {
        email: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            email: 'El usuario debe ser un correo valido <i class="fas fa-exclamation-triangle"></i>',
        },
        password: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
}) (jQuery);