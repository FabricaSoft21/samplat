(function($) {
    var form = $("#agregarUser");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        NombreCompleto: {
            required: true,
            letterswithbasicpunc: true,
        },
        email: {
            required: true,
            email: true,
        },
        password: {
            required: true,
        },
        Area: {
            required: true,
        },
        Rol: {
            required: true,
        },
        TipoDoc: {
            required: true,
        },
        NumeroDoc: {
            required: true,
            digits: true,
        },
        Celular: {
            required: true,
            digits: true,
        },
    },
    messages : {
        NombreCompleto: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            letterswithbasicpunc: 'Solo letras <i class="fas fa-exclamation-triangle"></i>',
        },
        email: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            email: 'Correo no es valido <i class="fas fa-exclamation-triangle"></i>',
        },
        password: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Area: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        Rol: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        TipoDoc: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
        },
        NumeroDoc: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            digits: 'Solo se permiten números enteros <i class="fas fa-exclamation-triangle"></i>'
        },
        Celular: {
            required: 'Campo requerido <i class="fas fa-exclamation-triangle"></i>',
            digits: 'Solo se permiten números enteros <i class="fas fa-exclamation-triangle"></i>'
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});

})(jQuery);