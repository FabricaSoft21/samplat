(function($) {
    var form = $("#tiempoValidate1");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Tiempo: {
            required: true,
            number: true,
            min: 0,
            max: 744,
        },
    },
    messages : {
        Tiempo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            number: 'Solo numeros <i class="fas fa-exclamation-triangle"></i>',
            min: 'El tiempo debe de ser mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            max: 'El tiempo debe de ser menor a 744 <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});

})(jQuery);

(function($) {
    var form = $("#tiempoValidate2");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Tiempo: {
            required: true,
            number: true,
            min: 0,
            max: 744,
        },
    },
    messages : {
        Tiempo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            number: 'Solo numeros <i class="fas fa-exclamation-triangle"></i>',
            min: 'El tiempo debe de ser mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            max: 'El tiempo debe de ser menor a 744 <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});

})(jQuery);

(function($) {
    var form = $("#tiempoValidate3");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Tiempo: {
            required: true,
            number: true,
            min: 0,
            max: 744,
        },
    },
    messages : {
        Tiempo: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            number: 'Solo numeros <i class="fas fa-exclamation-triangle"></i>',
            min: 'El tiempo debe de ser mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
            max: 'El tiempo debe de ser menor a 744 <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});

})(jQuery);