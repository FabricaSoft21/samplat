(function($) {
    var form = $("#categoriaValidate");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Nombre: {
            required: true,
            letterswithbasicpunc: true,
        },
    },
    messages : {
        Nombre: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
            letterswithbasicpunc: 'Solo letras <i class="fas fa-exclamation-triangle"></i>'
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});

})(jQuery);