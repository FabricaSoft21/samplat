(function($) {
    var form = $("#espacioValidate");
    form.validate({errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        Espacio: {
            required: true,
        },
    },
    messages : {
        Espacio: {
            required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
        },
    },
    onfocusout: function(element) {
        $(element).valid();
    },
});
})(jQuery);