<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class edificio extends Model
{
    protected $table='edificios';

	public $timestamps = false;
	protected $primaryKey = 'id_Edificio';
	protected $fillable = [
		'Nombre',
	];}
