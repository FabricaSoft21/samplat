<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class imagen extends Model
{
	protected $table='imagenes';

	public $timestamps = false;
	protected $primaryKey = 'id_Imagen';
	protected $fillable = [
		'Imagen_1', 'Imagen_2', 'Imagen_3', 'Imagen_4', 'Imagen_5',
	];  
}
