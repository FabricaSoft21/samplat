<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class equipo extends Model
{
	protected $table='equipos';

	public $timestamps = false;
	protected $primaryKey = 'id_Equipo';
	protected $fillable = [
		'Codigo', 'Modelo', 'Serie', 'Dependencia', 'Placa', 'PSB', 'Categoria', 'Marca', 'Tipo',
	];
}
