<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class solicitudMantenimiento extends Model
{
	protected $table='solicitud_mantenimientos';

	public $timestamps = false;
	protected $primaryKey = 'id_Solicitud';
	protected $fillable = [
		'NombreCompleto', 'Email', 'Prioridad', 'Categoria', 'Edificio', 'Espacio', 'Piso', 'Descripcion', 'InsumoAsignado', 'Imagen', 'Estado', 'Tecnico', 'Fecha_solicitud', 'Fecha_fin', 'Fecha_asignacion',
	];
}
