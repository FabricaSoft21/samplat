<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class insumos_sobrante extends Model
{
	protected $table='insumos_sobrantes';

	public $timestamps = false;
	protected $primaryKey = 'id_Insumo_Sobrante';
	protected $fillable = [
		'id_Ficha', 'id_Insumo', 'CantidadSobrante',
	];  
}
