<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categoria extends Model
{
    protected $table='categorias';

	public $timestamps = false;
	protected $primaryKey = 'id_Categoria';
	protected $fillable = [
		'Nombre',
	];
}
