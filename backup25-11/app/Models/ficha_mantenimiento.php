<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ficha_mantenimiento extends Model
{
	protected $table='ficha_mantenimientos';

	public $timestamps = false;
	protected $primaryKey = 'id_Ficha';
	protected $fillable = [
		'TipoMante', 'Observacion', 'id_Solicitud', 'ImagenSoli',
	];
}
