<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class img_mantenimiento extends Model
{
	protected $table='img_mantenimientos';

	public $timestamps = false;
	protected $primaryKey = 'id_Img';
	protected $fillable = [
		'Imagen_1', 'Imagen_2', 'Imagen_3', 'Imagen_4', 'Imagen_5',
	]; 
}
