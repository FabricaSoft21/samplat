<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class marca extends Model
{
	protected $table='marcas';

	public $timestamps = false;
	protected $primaryKey = 'id_Marca';
	protected $fillable = [
		'Marca',
	];
}
