<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class piso extends Model
{
    protected $table='pisos';

	public $timestamps = false;
	protected $primaryKey = 'id_Piso';
	protected $fillable = [
		'Piso',
	];
}
