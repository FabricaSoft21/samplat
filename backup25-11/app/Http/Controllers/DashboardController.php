<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\prioridad;
use App\Models\solicitudMantenimiento;

class DashboardController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('Supervisor');
	}

	public function index()
	{
		$solic = solicitudMantenimiento::count("solicitud_Mantenimientos.id_Solicitud");
		$solicA = solicitudMantenimiento::where("solicitud_Mantenimientos.Estado", "=" , "Abierto")->count();
		$solicB = solicitudMantenimiento::where("solicitud_Mantenimientos.Estado", "=" , "Asignado")->count();
		$solicC = solicitudMantenimiento::where("solicitud_Mantenimientos.Estado", "=" , "Pendiente")->count();
		$solicD = solicitudMantenimiento::where("solicitud_Mantenimientos.Estado", "=" , "En proceso")->count();
		$solicE = solicitudMantenimiento::where("solicitud_Mantenimientos.Estado", "=" , "En verificación")->count();
		$verifi = solicitudMantenimiento::where("solicitud_Mantenimientos.Estado", "=" , "Verificado")->count();
		$prioridadA = solicitudMantenimiento::where("solicitud_Mantenimientos.Prioridad", "=" , "1")->count();
		$prioridadB = solicitudMantenimiento::where("solicitud_Mantenimientos.Prioridad", "=" , "2")->count();
		$prioridadC = solicitudMantenimiento::where("solicitud_Mantenimientos.Prioridad", "=" , "3")->count();


		return view('dashboard.index', compact('prioridadA', 'prioridadB', 'prioridadC','solicA', 'solicB', 'solicC', 'solicD', 'solicE', 'solic','verifi'));
	}
}
