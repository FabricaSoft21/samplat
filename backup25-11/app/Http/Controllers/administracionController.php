<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\categoria;
use App\Models\edificio;
use App\Models\piso;
use App\Models\prioridad;
use App\Models\insumo;
use App\Models\espacio;

class administracionController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('Supervisor');
	}

	public function index()
	{
		$categorias = categoria::all();
		$edificios = edificio::all();
		$pisos = piso::all();
		$prioridad = prioridad::all();
		$insumo = insumo::all();
		$espacio = espacio::all();
		$i = 1;
		$e = 1;
		$u = 1;
		$p = 1;
		$q = 1;
		$w = 1;

		return view('administracion.index', compact('categorias', 'edificios', 'pisos', 'prioridad', 'insumo', 'espacio', 'i', 'e', 'u', 'p', 'q', 'w'));
	}

	public function categoriaStore(Request $request)
	{
		$categorias = new categoria();

		$categorias->Nombre=$request->input('Nombre');
		$categorias->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function edificioStore(Request $request)
	{
		$edificios = new edificio();

		$edificios->Nombre=$request->input('Nombre');
		$edificios->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function pisoStore(Request $request)
	{
		$Pisos = new piso();

		$Pisos->Piso=$request->input('Piso');
		$Pisos->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function regisEspacio(Request $request)
	{
		$espacio = new espacio();

		$espacio->Espacio=$request->input('Espacio');
		$espacio->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function prioridadUpdate(Request $request, $id)
	{
		$prioridad = prioridad::findOrFail($id);

		$prioridad->Tiempo=$request->input('Tiempo');
		$prioridad->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function categoriaUpdate(Request $request, $id)
	{
		$categorias = categoria::findOrFail($id);

		$categorias->Nombre=$request->input('Nombre');
		$categorias->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function edificioUpdate(Request $request, $id)
	{
		$edificios = edificio::findOrFail($id);

		$edificios->Nombre=$request->input('Nombre');
		$edificios->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function pisoUpdate(Request $request, $id)
	{
		$Pisos = piso::findOrFail($id);

		$Pisos->Piso=$request->input('Piso');
		$Pisos->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function espacioUpdate(Request $request, $id)
	{
		$espacio = espacio::findOrFail($id);

		$espacio->Espacio=$request->input('Espacio');
		$espacio->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

}
