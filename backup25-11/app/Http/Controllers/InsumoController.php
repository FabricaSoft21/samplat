<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\insumo;
use App\Models\insumo_solicitud;
use App\Models\solicitudMantenimiento;
use Illuminate\Validation\ValidationException;

class InsumoController extends Controller
{
	public function insumoStore(Request $request)
	{
		$insumo = new insumo();

		$insumo->Insumo=$request->input('Insumo');
		$insumo->Cantidad=$request->input('Cantidad');
		$insumo->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function insumoUpdate(Request $request, $id)
	{
		$insumo = insumo::findOrFail($id);

		$insumo->Insumo=$request->input('Insumo');
		$cantidad = $request->input('Cantidad');
		$op = $insumo->Cantidad+$cantidad;
		$insumo->Cantidad=$op;
		$insumo->save();
        // dd($request->all());
		return redirect()->route('Administracion');
	}

	public function solicInsumo(Request $request, $id)
	{
		$solMant = solicitudMantenimiento::findOrFail($id);

		$input = $request->all();
		$busCan = insumo::find($input["Insumo"]);

		$inputs = $input["Cantidad"];
		$in = array_map('intval', $inputs);

		$insid = $input["Insumo"];
		$insna = array_map('intval', $insid);

		foreach ($busCan as $busCans) { 
			$ar[] = $busCans->Cantidad;
			$na[] = $busCans->Insumo;
		}
		
		for ($b=0; $b < count($ar); $b++) {
			if ($in[$b] > $ar[$b]) {
				alert()->error('La cantidad utilizada de '.$na[$b].', es superior a '.$ar[$b].'.','Cantidad en Stock');
				return redirect()->route('home');
			}
		}

		for ($i=0; $i < count($ar); $i++) {
			if ($in[$i] < $ar[$i]){
				$solMantIns = 1;
				$solMant->InsumoAsignado=$solMantIns;
				$solMant->save();

				$idInsu = $request->input('Insumo');
				$idSol = $request->input('id_Solicitud');
				$cant = $request->input('Cantidad');

				foreach ($request->input('Insumo') as $key => $value)
				{
					$insumoSol = new insumo_solicitud();
					$insumoSol->id_Insumo=$idInsu[$key];
					$insumoSol->CantidadUsada=$cant[$key];
					$insumoSol->id_Solicitud=$idSol;

					$insumoSol->save();
				}

				$insu = insumo::all();
				$insumo = insumo_solicitud::all();

				foreach ($insu as $insus)
				{
					foreach ($insumo as $insumos)
					{
						if ($insus->id_Insumo === $insumos->id_Insumo && $insumos->id_Solicitud === $solMant->id_Solicitud)
						{
							$op = $insus->Cantidad-$insumos->CantidadUsada;

							$insus->Cantidad = $op;
							$insus->save();
						}
					}
				}
				alert()->success('Los insumos/herramientas se registraron correctamente','Registro Exitoso');
				return redirect()->route('home');
			}
		}
	}

	public function solicInsumos(Request $request, $id)
	{
		$solMant = solicitudMantenimiento::findOrFail($id);

		$input = $request->all();
		$busCan = insumo::find($input["Insumo"]);

		$inputs = $input["Cantidad"];
		$in = array_map('intval', $inputs);

		$insid = $input["Insumo"];
		$insna = array_map('intval', $insid);

		foreach ($busCan as $busCans) { 
			$ar[] = $busCans->Cantidad;
			$na[] = $busCans->Insumo;
		}
		
		for ($b=0; $b < count($ar); $b++) {
			if ($in[$b] > $ar[$b]) {
				alert()->error('La cantidad utilizada de '.$na[$b].', es superior a '.$ar[$b].'.','Cantidad en Stock');
				return redirect()->route('mantenimiento.index');
			}
		}

		for ($i=0; $i < count($ar); $i++) {
			if ($in[$i] < $ar[$i]){
				$solMantIns = 1;
				$solMant->InsumoAsignado=$solMantIns;
				$solMant->save();

				$idInsu = $request->input('Insumo');
				$idSol = $request->input('id_Solicitud');
				$cant = $request->input('Cantidad');
				foreach ($request->input('Insumo') as $key => $value)
				{
					$insumoSol = new insumo_solicitud();
					$insumoSol->id_Insumo=$idInsu[$key];
					$insumoSol->CantidadUsada=$cant[$key];
					$insumoSol->id_Solicitud=$idSol;

					$insumoSol->save();
				}

				$insu = insumo::all();
				$insumo = insumo_solicitud::all();

				foreach ($insu as $insus)
				{
					foreach ($insumo as $insumos)
					{
						if ($insus->id_Insumo === $insumos->id_Insumo && $insumos->id_Solicitud === $solMant->id_Solicitud)
						{
							$op = $insus->Cantidad-$insumos->CantidadUsada;

							$insus->Cantidad = $op;
							$insus->save();
						}
					}
				}
				alert()->success('Los insumos/herramientas se registraron correctamente','Registro Exitoso');
				return redirect()->route('mantenimiento.index');
			}
		}

	}
}
