<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ficha_mantenimiento;
use App\Models\img_mantenimiento;
use App\Models\insumo;
use App\Models\insumos_sobrante;
use App\Models\solicitudMantenimiento;

class FichaController extends Controller
{
	public function fichaMante(Request $request, $id)
	{
		$imagen = new img_mantenimiento();
		$ficha = new ficha_mantenimiento();
		$solicitud = solicitudMantenimiento::findOrFail($id);

        // Tabla imagenes mantenimiento
		$insert = null;

		if($request->hasfile('ImagenSoli'))
		{
			foreach($request->file('ImagenSoli') as $file)
			{
				$filename = time().$file->getClientOriginalName();
				$file->move('ImgMantenimiento',$filename);
				$insert[]['Imagen_1'] = $filename;
				$insert[]['Imagen_2'] = $filename;
				$insert[]['Imagen_3'] = $filename;
				$insert[]['Imagen_4'] = $filename;
				$insert[]['Imagen_5'] = $filename;
			}

			if (isset($insert['24'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']+$insert['12']+$insert['18']+$insert['24']);

			}elseif (isset($insert['18'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']+$insert['12']+$insert['18']);

			}elseif (isset($insert['12'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']+$insert['12']);

			}elseif (isset($insert['6'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']);

			}elseif (isset($insert['0'])) {
				$imagen = img_mantenimiento::insert($insert['0']);
			}
		}

		// Tabla ficha de mantenimiento
		$ficha->TipoMante=$request->input('TipoMante');
		$ficha->Observacion=$request->input('Observacion');
		$ficha->id_Solicitud=$request->input('id_Solicitud');
		$img = img_mantenimiento::all()->last()->id_Img;
		$ficha->ImagenSoli=$img;
		$ficha->save();


		$idInsu = $request->input('insumoSob');
		$cant = $request->input('cantidadSob');

		foreach ($request->input('insumoSob') as $key => $value)
		{
			$insumoSob = new insumos_sobrante();
			$insumoSob->id_Insumo=$idInsu[$key];
			$insumoSob->CantidadSobrante=$cant[$key];
			$fich = ficha_mantenimiento::all()->last()->id_Ficha;
			$insumoSob->id_Ficha=$fich;

			$insumoSob->save();
		}

		$insu = insumo::all();
		$fich = ficha_mantenimiento::all();
		$insumo = insumos_sobrante::all();
		
		foreach ($fich as $fichas) 
		{
			foreach ($insu as $insus)
			{
				foreach ($insumo as $insumos)
				{
					if ($insus->id_Insumo === $insumos->id_Insumo && $insumos->id_Ficha === $fichas->id_Ficha)
					{
						$op = $insus->Cantidad+$insumos->CantidadSobrante;

						$insus->Cantidad = $op;
						$insus->save();
					}
				}
			}
		}

		$solicitud->Estado=$request->input('EstadoSoli');
		$solicitud->save();

		return redirect()->route('home');
	}

	public function fichaMantes(Request $request, $id)
	{
		$imagen = new img_mantenimiento();
		$ficha = new ficha_mantenimiento();
		$solicitud = solicitudMantenimiento::findOrFail($id);

        //Tabla imagenes mantenimiento
		$insert = null;

		if($request->hasfile('ImagenSoli'))
		{
			foreach($request->file('ImagenSoli') as $file)
			{
				$filename = time().$file->getClientOriginalName();
				$file->move('ImgMantenimiento', $filename);
				$insert[]['Imagen_1'] = $filename;
				$insert[]['Imagen_2'] = $filename;
				$insert[]['Imagen_3'] = $filename;
				$insert[]['Imagen_4'] = $filename;
				$insert[]['Imagen_5'] = $filename;
			}

			if (isset($insert['24'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']+$insert['12']+$insert['18']+$insert['24']);

			}elseif (isset($insert['18'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']+$insert['12']+$insert['18']);

			}elseif (isset($insert['12'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']+$insert['12']);

			}elseif (isset($insert['6'])) {
				$imagen = img_mantenimiento::insert($insert['0']+$insert['6']);

			}elseif (isset($insert['0'])) {
				$imagen = img_mantenimiento::insert($insert['0']);
			}
		}

		// // Tabla ficha de mantenimiento
		$ficha->TipoMante=$request->input('TipoMante');
		$ficha->Observacion=$request->input('Observacion');
		$ficha->id_Solicitud=$request->input('id_Solicitud');
		$img = img_mantenimiento::all()->last()->id_Img;
		$ficha->ImagenSoli=$img;
		$ficha->save();


		$idInsu = $request->input('insumoSob');
		$cant = $request->input('cantidadSob');

		foreach ($request->input('insumoSob') as $key => $value)
		{
			$insumoSob = new insumos_sobrante();
			$insumoSob->id_Insumo=$idInsu[$key];
			$insumoSob->CantidadSobrante=$cant[$key];
			$fich = ficha_mantenimiento::all()->last()->id_Ficha;
			$insumoSob->id_Ficha=$fich;

			$insumoSob->save();
		}

		$insu = insumo::all();
		$fich = ficha_mantenimiento::all();
		$insumo = insumos_sobrante::all();
		
		foreach ($fich as $fichas) 
		{
			foreach ($insu as $insus)
			{
				foreach ($insumo as $insumos)
				{
					if ($insus->id_Insumo === $insumos->id_Insumo && $insumos->id_Ficha === $fichas->id_Ficha)
					{
						$op = $insus->Cantidad+$insumos->CantidadSobrante;

						$insus->Cantidad = $op;
						$insus->save();
					}
				}
			}
		}

		$solicitud->Estado=$request->input('EstadoSoli');
		$solicitud->save();

		return redirect()->route('mantenimiento.index');
	}
}