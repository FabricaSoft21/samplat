<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Rules\Uppercase;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use App\Models\roles;
use App\Models\tipo_documento;

class GestionUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('Supervisor');
    }

    public function index()
    {
        $rol = roles::all();
        $tipoDoc = tipo_documento::all();
        $user = User::all();
        $i = 1;

        return view('usuarios.usuario', compact('rol','tipoDoc','user', 'i'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'unique:users'],
        ]);
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();

        $user= new User();

        $user->email=$request->input('email');
        $user->password=Hash::make($request->input('password'));
        $user->Area=$request->input('Area');
        $user->NombreCompleto=$request->input('NombreCompleto');
        $user->TipoDoc=$request->input('TipoDoc');
        $user->NumeroDoc=$request->input('NumeroDoc');
        $user->Celular=$request->input('Celular');
        $user->Rol=$request->input('Rol');
        $user->save();

        // dd($request->all());
        return redirect()->route('listar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view ('usuarios.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user = User::findOrFail($id);
        
        $user->email=$request->input('email');
        if ($request->input('password_new') != null)
        {
            $user->password=Hash::make($request->input('password_new'));
        }
        $user->Area=$request->input('Area');
        $user->NombreCompleto=$request->input('NombreCompleto');
        $user->TipoDoc=$request->input('TipoDoc');
        $user->NumeroDoc=$request->input('NumeroDoc');
        $user->Celular=$request->input('Celular');
        $user->Estado=$request->input('Estado');
        $user->Rol=$request->input('Rol');
        $user->save();

        // dd($request->all());
        return redirect()->route('listar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
