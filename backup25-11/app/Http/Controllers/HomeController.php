<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\categoria;
use App\Models\piso;
use App\Models\edificio;
use App\Models\espacio;
use App\Models\imagen;
use App\Models\prioridad;
use App\Models\insumo;
use App\Models\insumo_solicitud;
use App\Models\solicitudMantenimiento;
use App\User;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        Carbon::setLocale('es');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $categoria = categoria::all();
        $piso = piso::all();
        $espacio = espacio::all();
        $edificio = edificio::all();
        $solMant = solicitudMantenimiento::all();
        $img = imagen::all();
        $prioridad = prioridad::all();
        $carbon = Carbon::now();
        $user = User::all();
        $insumo = insumo::all();
        $insumoSol = insumo_solicitud::all();

        return view('home', compact('categoria', 'piso', 'espacio', 'edificio', 'solMant', 'prioridad', 'user', 'insumo', 'insumoSol', 'img', 'carbon'));
    }
}
