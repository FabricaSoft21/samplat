<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\equipo;
use App\Models\marca;
use App\Models\tipo;
use App\Models\categoria;

class EquiposController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('Supervisor');
	}

	public function equipo()
	{
		$categorias = categoria::all();
		$marca = marca::all();
		$tipo = tipo::all();
		$equipo = equipo::all();
		$i = 1;

		return view('equipos.index', compact('categorias', 'marca', 'tipo', 'equipo', 'i'));
	}

	public function storeEquipo(Request $request)
	{
		$equipo = new equipo();

		$equipo->Codigo=$request->input('Codigo');
		$equipo->Modelo=$request->input('Modelo');
		$equipo->Serie=$request->input('Serie');
		$equipo->Dependencia=$request->input('Dependencia');
		$equipo->Placa=$request->input('Placa');
		$equipo->PSB=$request->input('PSB');
		$equipo->Categoria=$request->input('Categoria');
		$equipo->Marca=$request->input('Marca');
		$equipo->Tipo=$request->input('Tipo');
		$equipo->save();

		return redirect()->route('Equipos');
	}

	public function storeMarca(Request $request)
	{
		$marca = new marca();

		$marca->Marca=$request->input('Marca');
		$marca->save();

		return redirect()->route('Equipos');
	}

	public function storeTipo(Request $request)
	{
		$tipo = new tipo();

		$tipo->Tipo=$request->input('Tipo');
		$tipo->save();

		return redirect()->route('Equipos');
	}

	public function equipoUpdate(Request $request, $id)
	{
		$equipo = equipo::findOrFail($id);

		$equipo->Codigo=$request->input('Codigo');
		$equipo->Modelo=$request->input('Modelo');
		$equipo->Serie=$request->input('Serie');
		$equipo->Dependencia=$request->input('Dependencia');
		$equipo->Placa=$request->input('Placa');
		$equipo->PSB=$request->input('PSB');
		$equipo->Categoria=$request->input('Categoria');
		$equipo->Marca=$request->input('Marca');
		$equipo->Tipo=$request->input('Tipo');
		$equipo->save();
		
        // dd($request->all());
		return redirect()->route('Equipos');
	}
}
