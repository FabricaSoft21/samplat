<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichaMantenimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha_mantenimientos', function (Blueprint $table) {
            $table->id('id_Ficha');
            $table->string('TipoMante');
            $table->string('Observacion');
            $table->unsignedBigInteger('id_Solicitud');
            $table->foreign('id_Solicitud')->references('id_Solicitud')->on('solicitud_mantenimientos');
            $table->unsignedBigInteger('ImagenSoli');
            $table->foreign('ImagenSoli')->references('id_Img')->on('img_mantenimientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ficha_mantenimientos');
    }
}
