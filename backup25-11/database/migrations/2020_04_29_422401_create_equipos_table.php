<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->id('id_Equipo');
            $table->string('Codigo');
            $table->string('Modelo');
            $table->string('Serie');
            $table->string('Dependencia');
            $table->string('Placa');
            $table->boolean('PSB');
            $table->unsignedBigInteger('Categoria');
            $table->foreign('Categoria')->references('id_Categoria')->on('categorias');
            $table->unsignedBigInteger('Marca');
            $table->foreign('Marca')->references('id_Marca')->on('marcas');
            $table->unsignedBigInteger('Tipo');
            $table->foreign('Tipo')->references('id_Tipo')->on('tipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipos');
    }
}
