<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(TipoDocsSeeder::class);

        $this->call(RolesSeeder::class);

        $this->call(UserSeeder::class);

        $this->call(PisosSeeder::class);
        
        $this->call(PrioridadSeeder::class);
    }
}
