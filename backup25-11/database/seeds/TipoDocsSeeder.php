<?php

use Illuminate\Database\Seeder;

class TipoDocsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('tipo_documento')->insert([
    		'Nombre' => 'CC',
    	]);
    	DB::table('tipo_documento')->insert([
    		'Nombre' => 'CE',
    	]);
    	DB::table('tipo_documento')->insert([
    		'Nombre' => 'PA',
    	]);
    }
}
