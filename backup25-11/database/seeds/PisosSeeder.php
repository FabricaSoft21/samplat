<?php

use Illuminate\Database\Seeder;

class PisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pisos')->insert([
    		'Piso' => 'Piso 1',
    	]);
    	DB::table('pisos')->insert([
    		'Piso' => 'Piso 2',
    	]);
    	DB::table('pisos')->insert([
    		'Piso' => 'Piso 3',
    	]);
    	DB::table('pisos')->insert([
    		'Piso' => 'Piso 4',
    	]);
    	DB::table('pisos')->insert([
    		'Piso' => 'Piso 5',
    	]);
    }
}
