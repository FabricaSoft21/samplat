<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\categoria;
use App\Models\edificio;
use App\Models\espacio;
use App\Models\piso;
use App\Models\prioridad;
use Carbon\Carbon;

Route::get('/', function () {
	$categorias = categoria::all();
	$edificios = edificio::all();
	$espacio = espacio::all();
	$pisos = piso::all();
	$prioridad = prioridad::all();
	$carbon = carbon::now();

	return view('welcome', compact('categorias', 'edificios', 'espacio', 'pisos', 'prioridad','carbon'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Rutas Solicitud de Mantenimientos
Route::resource('mantenimiento','SolicitudMantenimientoController');
Route::put('solicitud/mantenimiento','SolicitudMantenimientoController@store')->name('solicitudMante');
Route::put('/solicitud/{id}','SolicitudMantenimientoController@update')->name('asigTec');
Route::put('/solicitud/mantenimiento/{id}','SolicitudMantenimientoController@updateTec')->name('asigTec2');
Route::put('/solicitud/caso/{id}','SolicitudMantenimientoController@EstadoCaso')->name('EstadoCaso');

// Rutas Gestion Usuario
Route::resource('usuarios','GestionUsersController')->middleware('Supervisor');
Route::put('/usuarios/usuario/store','GestionUsersController@store')->name('registrar');
Route::get('/usuarios','GestionUsersController@index')->name('listar');

// Rutas Administracion
Route::get('administracion/index', 'administracionController@index')->name('Administracion')->middleware('Supervisor');
Route::put('administracion/categoria', 'administracionController@categoriaStore')->name('regisCategoria');
Route::put('administracion/edificio', 'administracionController@edificioStore')->name('regisEdificio');
Route::put('administracion/piso', 'administracionController@pisoStore')->name('regisPiso');
Route::put('administracion/espacio', 'administracionController@regisEspacio')->name('regisEspacio');
Route::put('/administracion/categoria/{id}', 'administracionController@categoriaUpdate')->name('categoriaUpdate');
Route::put('/administracion/edificio/{id}', 'administracionController@edificioUpdate')->name('edificioUpdate');
Route::put('/administracion/piso/{id}', 'administracionController@pisoUpdate')->name('pisoUpdate');
Route::put('/administracion/espacio/{id}', 'administracionController@espacioUpdate')->name('espacioUpdate');
Route::put('/administracion/prioridad/{id}', 'administracionController@prioridadUpdate')->name('prioridadUpdate');

// Rutas Insumos
Route::put('administracion/insumo', 'InsumoController@insumoStore')->name('regisInsumo');
Route::put('/administracion/insumo/{id}', 'InsumoController@insumoUpdate')->name('insumoUpdate');
Route::put('/administracion/insumoSol/{id}', 'InsumoController@solicInsumo')->name('solicInsumo');
Route::put('/administracion/insumoSols/{id}', 'InsumoController@solicInsumos')->name('solicInsumos');

// Rutas Gestion Equipo
Route::get('equipos/index', 'EquiposController@equipo')->name('Equipos')->middleware('Supervisor');
Route::put('equipos/equipo', 'EquiposController@storeEquipo')->name('storeEquipo');
Route::put('equipos/marca', 'EquiposController@storeMarca')->name('storeMarca');
Route::put('equipos/tipo', 'EquiposController@storeTipo')->name('storeTipo');
Route::put('/equipos/equipo/{id}', 'EquiposController@equipoUpdate')->name('equipoUpdate');

// Rutas Cerrar Caso
Route::put('/ficha/cerrar/{id}', 'FichaController@fichaMante')->name('fichaMante');
Route::put('/ficha/cerrarCaso/{id}', 'FichaController@fichaMantes')->name('fichaMantes');

// Rutas Dashboard
Route::get('dashboard', 'DashboardController@index')->middleware('Supervisor');