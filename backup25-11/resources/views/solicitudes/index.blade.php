@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<link rel="stylesheet" href="{{asset('css/datatable.css')}}">
<link rel="stylesheet" href="{{asset('css/modalStyle.css')}}">
<link rel="stylesheet" href="{{asset('css/file-input.css') }}">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10 text-center"><h1 style="margin-bottom: 6%"><strong>GESTIÓN DE SOLICITUDES</strong></h1></div>
		<div class="col-md-10 tablestyle table-responsive">
			<table id="listSolici" class="table table-light table-striped">
				<thead class="thead-color">
					<tr>
						<th class="text-center th-cl">N°</th>
						<th class="text-center">Solicitante</th>
						<th class="text-center">Categoria y Ubicación</th>
						<th class="text-center">Fecha y Hora de Solicitud</th>
						<th class="text-center">Estado</th>
						<th class="text-center th-cr">Opciones</th>
					</tr>
				</thead>
				<tbody id="listSoliciBody">
					@foreach($solicitudes as $solic)
					@if(Auth::user()->Rol == 1)
					<tr>
						<td class="text-center">{{$i++}}</td>
						<td class="text-center">{{$solic->NombreCompleto}}</td>
						@foreach($categoria as $catego)
						@foreach($edificio as $edifi)
						@if($catego->id_Categoria === $solic->Categoria && $edifi->id_Edificio === $solic->Edificio)
						<td class="text-center">{{$catego->Nombre}} - {{$edifi->Nombre}}</td>
						@endif
						@endforeach
						@endforeach
						<td class="text-center">{{$solic->Fecha_solicitud}}</td>
						@if($solic->Prioridad === 1)
						<td class="text-center"><div style="background-color: #ff0000 !important;">{{$solic->Estado}}</div></td>
						@elseif($solic->Prioridad === 2)
						<td class="text-center"><div style="background-color: #ffc000 !important;">{{$solic->Estado}}</div></td>
						@elseif($solic->Prioridad === 3)
						<td class="text-center"><div style="background-color: #92d050 !important;">{{$solic->Estado}}</div></td>
						@endif
						<td class="text-center td-w">
							<a href="#" class="btn btn-view" data-toggle="modal" data-target="#ListDetalle{{$solic->id_Solicitud}}"><img src="{{ asset('img/eye.svg') }}" class="fa-siz"></a>
							@if($solic->Estado === "Abierto" && Auth::user()->Rol === 1)
							<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#ListTecnic{{$solic->id_Solicitud}}"><img src="{{ asset('img/pen.svg') }}" class="fa-siz"></a>
							@endif
							@foreach($ficha as $fichas)
							@if($fichas->id_Solicitud === $solic->id_Solicitud)
							@if($solic->Estado === "Pendiente" || $solic->Estado === "En proceso" || $solic->Estado === "En verificación" && Auth::user()->Rol === 1)
							<a href="#" class="btn btn-view" data-toggle="modal" data-target="#verSoli{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}"><img src="{{ asset('img/excla.svg') }}" class="fa-siz"></a>
							@endif
							@endif
							@endforeach
						</td>
					</tr>
					@endif
					@if(Auth::user()->Rol === 2 && Auth::user()->id === $solic->Tecnico)
					<tr>
						<td class="text-center">{{$i++}}</td>
						<td class="text-center">{{$solic->NombreCompleto}}</td>
						@foreach($categoria as $catego)
						@foreach($edificio as $edifi)
						@if($catego->id_Categoria === $solic->Categoria && $edifi->id_Edificio === $solic->Edificio)
						<td class="text-center">{{$catego->Nombre}} - {{$edifi->Nombre}}</td>
						@endif
						@endforeach
						@endforeach
						<td class="text-center">{{$solic->Fecha_solicitud}}</td>
						@if($solic->Prioridad === 1)
						<td class="text-center"><div style="background-color: #ff0000 !important;">{{$solic->Estado}}</div></td>
						@elseif($solic->Prioridad === 2)
						<td class="text-center"><div style="background-color: #ffc000 !important;">{{$solic->Estado}}</div></td>
						@elseif($solic->Prioridad === 3)
						<td class="text-center"><div style="background-color: #92d050 !important;">{{$solic->Estado}}</div></td>
						@endif
						<td class="text-center td-w">
							<a href="#" class="btn btn-view" data-toggle="modal" data-target="#ListDetalle{{$solic->id_Solicitud}}"><img src="{{ asset('img/eye.svg') }}" class="fa-siz"></a>
							@if($solic->Estado === "Asignado" && Auth::user()->Rol === 2 && $solic->InsumoAsignado === 0)
							<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#Insumos{{$solic->id_Solicitud}}"><img src="{{ asset('img/correcto.svg') }}" class="fa-siz"></a>
							@endif
							@if($solic->Estado === "Asignado" && Auth::user()->Rol === 2 && $solic->InsumoAsignado != 0)
							<a href="#" class="btn btn-edit" data-toggle="modal" data-target="#CerrarCaso{{$solic->id_Solicitud}}"><img src="{{ asset('img/cerrar.svg') }}" class="fa-siz"></a>
							@endif
						</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
			
			@foreach($solicitudes as $solic)
			{{-- Modal Listar Tecnicos --}}
			<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="TecniModalLabel" id="ListTecnic{{$solic->id_Solicitud}}">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>LISTA DE TÉCNICOS</h1>
						</div>
						<div class="modal-body">
							<table id="ListTec" class="table table-light table-striped ListTec">
								<thead class="thead-color">
									<tr>
										<th class="text-center th-cl">Nombre Completo</th>
										<th class="text-center">Área Laboral</th>
										<th class="text-center th-cr">Acciones</th>
									</tr>
								</thead>
								<tbody>
									@foreach($user as $users)
									@if($users->Rol === 2)
									<tr>
										<td class="text-center">{{$users->NombreCompleto}}</td>
										<td class="text-center">{{$users->Area}}</td>
										<td class="text-center">
											<form method="POST" action="{{route('asigTec2',[$solic->id_Solicitud])}}">
												{{ csrf_field() }}
												@method('PUT')
												<input type="hidden" name="Estado" value="Asignado">
												<input type="hidden" name="Tecnico" value="{{$users->id}}">
												<input type="hidden" name="EmailUser" value="{{$solic->Email}}">
												<input type="hidden" name="NombreTec" value="{{$users->NombreCompleto}}">
												<button type="submit" class="btn btn-edit"><img src="{{ asset('img/datos-verificados.svg') }}" class="fa-siz"></button>
											</form>
										</td>
									</tr>
									@endif
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="modal-footer" style="margin-top: 15px;">
							<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
						</div>
					</div>
				</div>
			</div>
			{{-- Fin Modal --}}

			{{-- Modal Detalles Solicitud --}}
			<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="TecniModalLabel" id="ListDetalle{{$solic->id_Solicitud}}">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>DETALLE DE LA SOLICITUD</h1>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-6">
									<h4>Nombre del Solicitante</h4>
									<input type="text" class="form-control" name="NombreCompleto" id="NombreCompleto" value="{{$solic->NombreCompleto}}" disabled="">
								</div>
								<div class="col-6">
									<h4>Correo Electronico del Solicitante</h4>
									<input type="email" class="form-control" name="Email" id="Email" value="{{$solic->Email}}" disabled=""><br>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<h4>Categoria de la Solicitud</h4>
									@foreach($categoria as $cate)
									@if($cate->id_Categoria === $solic->Categoria)
									<input type="text" class="form-control" name="Nombre" id="Nombre" value="{{$cate->Nombre}}" disabled="">
									@endif
									@endforeach
								</div>
								<div class="col-6">
									<h4>Técnico Asignado</h4>
									<input type="text" class="form-control" name="Tecnico" id="Tecnico"@foreach($user as $users) @if($users->id === $solic->Tecnico) value="{{$users->NombreCompleto}}"@endif @endforeach placeholder="No hay técnico asignado" disabled=""><br>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<h4>Estado</h4>
									<input type="text" class="form-control" name="Estado" id="Estado" value="{{$solic->Estado}}" disabled="">
								</div>
								<div class="col-6">
									<h4>Prioridad</h4>
									@foreach($prioridad as $priori)
									@if($priori->id_Prioridad === $solic->Prioridad)
									<input type="text" class="form-control" name="Prioridad" id="Prioridad" value="{{$priori->Prioridad}}" disabled=""><br>
									@endif
									@endforeach
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<h4>Ubicacion del Caso</h4>
									@foreach($edificio as $edificios)
									@foreach($piso as $pisos)
									@foreach($espacio as $espacios)
									@if($solic->Edificio === $edificios->id_Edificio)
									@if($solic->Piso === $pisos->id_Piso)
									@if($solic->Espacio === $espacios->id_Espacio)
									<input type="text" class="form-control" name="NombreCompleto" id="NombreCompleto" value="{{$edificios->Nombre}} - {{$pisos->Piso}} - {{$espacios->Espacio}}" disabled="">
									@endif
									@endif
									@endif
									@endforeach
									@endforeach
									@endforeach
								</div>
								<div class="col-6">
									<h4>Fecha de solicitud del Caso</h4>
									<input type="text" class="form-control" name="Fecha_solicitud" id="Fecha_solicitud" value="{{$solic->Fecha_solicitud}}" disabled=""><br>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<h4>Descripcion del caso</h4>
									<textarea class="form-control" disabled="" rows="3">{{$solic->Descripcion}}</textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer" style="margin-top: 15px;">
							<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
						</div>
					</div>
				</div>
			</div>
			{{-- Fin Modal --}}

			{{-- Modal Registrar Insumos --}}
			<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="insumoModalLabel" id="Insumos{{$solic->id_Solicitud}}">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						{{-- formulario Insumo --}}
						<div class="modal-header" style="justify-content: center;">
							<h1>ASIGANAR INSUMOS AL CASO</h1>
						</div>
						<div class="modal-body">
							<form class="" id="asignarInsumo{{$solic->id_Solicitud}}" method="POST" action="{{route('solicInsumos',[$solic->id_Solicitud])}}">
								{{ method_field('PUT') }}
								{{ csrf_field() }}
								<div class="row field_wrapper">
									<div class="col-5">
										<input type="hidden" name="id_Solicitud" value="{{$solic->id_Solicitud}}">
										<label for="Insumo" class="col-md-12 form-label text-md-left">Insumo<span class="red">*</span></label>
										<div class="col-md-12">
											<select class="form-control" id="Insumos" name="Insumo[]" value="{{ old('Insumo') }}" required="">
												<option value="">Seleccionar</option>
												@foreach($insumo as $insumos)
												<option value="{{$insumos->id_Insumo}}">{{$insumos->Insumo}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-5">
										<label for="Cantidad" class="col-md-12 form-label text-md-left">Cantidad<span class="red">*</span></label>
										<div class="col-md-12">
											<input id="Cantidad" type="number" class="form-control" name="Cantidad[]" value="{{ old('Cantidad') }}" autocomplete="Cantidad" autofocus placeholder="Ingrese la cantidad de insumos" required="" min="1" >
										</div>
									</div>
									<a href="javascript:void(0);" class="col add_button" title="Add field"><img class="imgAdd" src="{{ asset('img/interface.svg')}}"/></a>
								</div>
								<div class="modal-footer" style="margin-top: 15px;">
									<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
									<button type="submit" class="btn btn-env">
										Registrar
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			{{-- Fin Modal --}}

			{{-- Modal Cerrar Caso --}}
			<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="TecniModalLabel" id="CerrarCaso{{$solic->id_Solicitud}}">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>FICHA DEL MANTENIMIENTO</h1>
						</div>
						<div class="modal-body">
							{{-- Formulario Cerrar Caso --}}
							<form  id="fichaMantes{{$solic->id_Solicitud}}" method="POST" enctype="multipart/form-data" action="{{route('fichaMantes',[$solic->id_Solicitud])}}">
								{{ method_field('PUT') }}
								{{ csrf_field() }}
								<div class="row">
									<div class="col-5">
										<label for="TipoMante" class="col-md-12 form-label text-md-left">Tipo de mantenimiento<span class="red">*</span></label>
										<div class="col-md-12">
											<input type="hidden" name="id_Solicitud" value="{{$solic->id_Solicitud}}">
											<select class="form-control Mbott" id="TipoMante" name="TipoMante" value="{{ old('TipoMante') }}" >
												<option value="">Seleccionar</option>
												<option value="Preventivo">Preventivo</option>
												<option value="Correctivo">Correctivo</option>
												<option value="Predictivo">Predictivo</option>
											</select>
										</div>
										<label for="EstadoSoli" class="col-md-12 form-label text-md-left">Estado de la solicitud<span class="red">*</span></label>
										<div class="col-md-12">
											<select class="form-control Mbott" id="EstadoSoli" name="EstadoSoli" value="{{ old('EstadoSoli') }}" >
												<option value="">Seleccionar</option>
												<option value="Pendiente">Pendiente</option>
												<option value="En proceso">En proceso</option>
												<option value="En verificación">En verificación</option>
											</select>
										</div>
										<label for="InsumosUtil" class="col-md-12 form-label text-md-left">Insumos utilizados<span class="red">*</span></label>
										<div class="col-md-12">
											<textarea type="text" class="form-control Mbott" name="InsumosUtil" id="InsumosUtil" rows="8" disabled="">
												@foreach($insumoSol as $insumoSols)
												@foreach($insumo as $insumos)
												@if($insumoSols->id_Solicitud === $solic->id_Solicitud && $insumos->id_Insumo === $insumoSols->id_Insumo)
												{{$insumos->Insumo}} --> {{$insumoSols->CantidadUsada}}
												@endif
												@endforeach
												@endforeach
											</textarea>
										</div>
										<label for="ImagenSoli" class="col-md-12 form-label text-md-left">Adjuntar imagenes de la solicitud<span class="red">*</span></label>
										<div class="col-md-12">
											<input type="file" name="ImagenSoli[]" id="file-2{{$solic->id_Solicitud}}" class="inputfile inputfile-2" data-multiple-caption="{count} Imagenes seleccionadas" multiple accept="image/*" />
											<label for="file-2{{$solic->id_Solicitud}}">
												<span class="iborrainputfile">Adjuntar imagen</span>
											</label>
										</div>
									</div>
									<div class="col-7">
										<label for="Observacion" class="col-md-12 form-label text-md-left">Observaciones<span class="red">*</span></label>
										<div class="col-md-12">
											<textarea type="text" class="form-control Mbott" name="Observacion" id="Observacion" rows="6"></textarea>
										</div>
										<label for="insumoSob" class="col-md-12 form-label text-left">Insumos sobrantes<span class="red">*</span></label>
										@foreach($insumoSol as $insumoSols)
										@foreach($insumo as $insumos)
										@if($insumoSols->id_Solicitud === $solic->id_Solicitud && $insumos->id_Insumo === $insumoSols->id_Insumo)
										<div class="col-md-12">											
											<select class="form-control col-12 float-left Mbott" id="insumoSob{{$insumos->id_Insumo}}{{$solic->id_Solicitud}}" name="insumoSob[]" value="{{ old('insumoSob') }}">
												<option value="{{$insumos->id_Insumo}}">{{$insumos->Insumo}}</option>
											</select>
											<input id="cantidadSob{{$insumos->id_Insumo}}{{$solic->id_Solicitud}}" name="cantidadSob[] {{$insumos->id_Insumo}}" type="number" class="form-control Mbott"  value="0" min="0" max="{{$insumoSols->CantidadUsada}}" required="">
										</div>
										@endif
										@endforeach
										@endforeach
									</div>
								</div>
								<div class="modal-footer" style="margin-top: 15px;">
									<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
									<button type="submit" class="btn btn-env">
										Registrar
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			{{-- Fin Modal --}}

			@foreach($ficha as $fichas)
			{{-- Modal Validar Solicitud --}}
			<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="ValidarModalLabel" id="verSoli{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>VERIFICAR ESTADO DEL CASO</h1>
						</div>
						<form id="" class="" method="POST" enctype="multipart/form-data" action="{{route('EstadoCaso',[$solic->id_Solicitud])}}">
							{{ method_field('PUT') }}
							{{ csrf_field() }}
							<div class="modal-body">
								<input type="hidden" name="EmailUser" value="{{$solic->Email}}">
								@foreach($user as $users)
								@if($solic->Tecnico === $users->id)
								<input type="hidden" name="NombreTec" value="{{$users->NombreCompleto}}">
								@endif
								@endforeach
								<div class="row">
									<div class="col-6">
										<h4>Tipo de solicitud</h4>
										<input type="text" class="form-control" name="TipoMante" id="TipoMante" value="{{$fichas->TipoMante}}" disabled="">
									</div>
									<div class="col-6">
										<h4>Estado de la solicitud</h4>
										<input type="text" class="form-control" name="EstadoSoli" id="EstadoSoli" value="{{$solic->Estado}}" disabled=""><br>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
										<h4>Observaciones del caso</h4>
										<textarea class="form-control" disabled="" rows="5">{{$fichas->Observacion}}</textarea><br>
									</div>
								</div>
								<div class="row">
									<div class="col-6">
										<h4>Imagenes del caso</h4>
										@foreach($img as $imgs)
										@if($fichas->id_Ficha === $imgs->id_Img)
										<a href="#" data-toggle="modal" data-target="#ImgSlide{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}"><img class="img-St" src="../../ImgMantenimiento/{{$imgs->Imagen_1}}"></a>
										@endif
										@endforeach
									</div>
									<div class="col-6">
										<h4>Asignar estado</h4>
										<select class="form-control" id="Estado" name="Estado" value="{{ old('Estado') }}" >
											<option value="">Seleccionar</option>
											<option value="Pendiente">Pendiente</option>
											<option value="En proceso">En proceso</option>
											<option value="Verificado">Verificado</option>
										</select><br>
									</div>
								</div>
							</div>
							<div class="modal-footer" style="margin-top: 15px;">
								<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
								<button type="submit" class="btn btn-env">
									Registrar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			{{-- Fin Modal --}}

			{{-- Modal Slider --}}
			<div class="modal fade col-12 justify-content-center" tabindex="-1" role="dialog" aria-labelledby="SlideModalLabel" id="ImgSlide{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header" style="justify-content: center;">
							<h1>VER IMAGENES</h1>
						</div>
						<div class="modal-body">
							<div id="carouselExampleControls{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner">
									<div class="carousel-item active">
										@foreach($img as $imgs)
										@if($fichas->id_Ficha === $imgs->id_Img)
										<img @if($imgs->Imagen_1 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../ImgMantenimiento/{{$imgs->Imagen_1}}" @endif alt="First slide" class="img-slid">
										@endif
										@endforeach
									</div>
									<div class="carousel-item">
										@foreach($img as $imgs)
										@if($fichas->id_Ficha === $imgs->id_Img)
										<img @if($imgs->Imagen_2 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../ImgMantenimiento/{{$imgs->Imagen_2}}" @endif alt="Second slide" class="img-slid">
										@endif
										@endforeach
									</div>
									<div class="carousel-item">
										@foreach($img as $imgs)
										@if($fichas->id_Ficha === $imgs->id_Img)
										<img @if($imgs->Imagen_3 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../ImgMantenimiento/{{$imgs->Imagen_3}}" @endif alt="Third slide" class="img-slid">
										@endif
										@endforeach
									</div>
									<div class="carousel-item">
										@foreach($img as $imgs)
										@if($fichas->id_Ficha === $imgs->id_Img)
										<img @if($imgs->Imagen_4 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../ImgMantenimiento/{{$imgs->Imagen_4}}" @endif alt="Four slide" class="img-slid">
										@endif
										@endforeach
									</div>
									<div class="carousel-item">
										@foreach($img as $imgs)
										@if($fichas->id_Ficha === $imgs->id_Img)
										<img @if($imgs->Imagen_5 == null) src="{{ asset('img/SAM1000X1000.png') }}" @else src="../../ImgMantenimiento/{{$imgs->Imagen_5}}" @endif alt="Five slide" class="img-slid">
										@endif
										@endforeach
									</div>
								</div>
								<a class="carousel-control-prev" href="#carouselExampleControls{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carouselExampleControls{{$solic->id_Solicitud}}{{$fichas->id_Ficha}}" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			{{-- Fin Slider --}}
			@endforeach

			@endforeach
		</div>
	</div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('js/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/jquery-validation/dist/additional-methods.min.js')}}"></script>
<script src="{{asset('js/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('js/minimalist-picker/dobpicker.js')}}"></script>
<script src="{{asset('js/jquery.pwstrength/jquery.pwstrength.js')}}"></script>
<script src="{{asset('js/custom-file-input.js')}}"></script>
<script>
	$(document).ready(function() {
		$('#listSolici').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('.ListTec').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.20/i18n/Spanish.json"
			}
		});
	});
</script>

<script>
	$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper

    var x = 1; //Initial field counter is 1
    var z = 1;
    var y = 1;
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            z++;
            y++;
            $(wrapper).append('<div><div class="col-5 float-left Mtop"><div class="col-md-12"><select class="form-control" id="Insumo'+z+'" name="Insumo[]'+z+'" value="{{ old('Insumo') }}" required><option value="">Seleccionar</option>@foreach($insumo as $insumos)<option value="{{$insumos->id_Insumo}}">{{$insumos->Insumo}}</option>@endforeach</select></div></div><div class="col-5 float-left Mtop"><div class="col-md-12"><input id="Cantidad'+y+'" type="number" class="form-control" name="Cantidad[]'+y+'" value="{{old('Cantidad')}}" autocomplete="Cantidad" autofocus placeholder="Ingrese la cantidad de insumos" required min="1"></div></div><a href="javascript:void(0);" class="remove_button" title="Remove field"><img class="imgRem" src="{{ asset('img/signs.svg')}}"/></a></div>'); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
    	e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>

@include ('sweet::alert')

@foreach($solicitudes as $solic)
<script>
	(function($) {
		var form = $("#fichaMantes{{$solic->id_Solicitud}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			TipoMante: {
				required: true,
			},
			EstadoSoli: {
				required: true,
			},
			Observacion: {
				required: true,
			},
		},
		messages : {
			TipoMante: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			EstadoSoli: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			Observacion: {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			@foreach($insumo as $insumos)
			'cantidadSob[] {{$insumos->id_Insumo}}': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor o igual a 0 <i class="fas fa-exclamation-triangle"></i>',
				max: 'La cantidad excede la utilizada <i class="fas fa-exclamation-triangle"></i>',
			},
			@endforeach
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});

	})(jQuery);
</script>

<script>
	(function($) {
		var form = $("#asignarInsumo{{$solic->id_Solicitud}}");
		form.validate({errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		rules: {
			'Cantidad[]': {

			},
		},
		messages : {
			'Insumo[]': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]2': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]3': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]4': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]5': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]6': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]7': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]8': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]9': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Insumo[]10': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]2': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]3': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]4': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]5': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]6': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]7': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]8': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]9': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
			'Cantidad[]10': {
				required: 'Este campo es requerido <i class="fas fa-exclamation-triangle"></i>',
				number: 'Ingrese un número valido <i class="fas fa-exclamation-triangle"></i>',
				min: 'Ingrese un número mayor a 0 <i class="fas fa-exclamation-triangle"></i>',
			},
		},
		onfocusout: function(element) {
			$(element).valid();
		},
	});

})(jQuery);
</script>
@endforeach

@endsection