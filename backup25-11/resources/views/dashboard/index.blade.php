@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="{{asset('css/datatable.css')}}">
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-10 text-center">
			<h1><strong>DASHBOARD</strong></h1><br>
			<div id="prioridad" class="col-12"></div>
			<div id="casos" class="col-12"></div>
		</div>
	</div>
</div>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
</script>
<script src = "https://code.highcharts.com/highcharts.js"></script>  
<script src = "https://code.highcharts.com/highcharts-3d.js"></script>
<script>
	$(document).ready(function() {  
		var chart = {      
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45,
				beta: 0
			}
		};
		var title = {
			text: 'Grafica de casos segun la prioridad'
		};
		var plotOptions = {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,

				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				}
			}
		};   
		var series = [{
			type: 'pie',
			name: 'Número de casos',
			data: [
			{
				name:'Total de casos',
				y:{{$solic}},
				color:'#36A2EB'
			},
			{
				name:'Casos de prioridad Alta',
				y:{{$prioridadA}},
				color:'#FF0000'
			},
			{
				name:'Casos de prioridad Media',
				y:{{$prioridadB}},
				color:'#FFC000'
			},
			{
				name:'Casos de prioridad Baja',
				y:{{$prioridadC}},
				color:'#92D050'
			},
			]
		}];     
		var json = {};   
		json.chart = chart;
		json.title = title;
		json.plotOptions = plotOptions;
		json.series = series;
		$('#prioridad').highcharts(json);
	});
</script>
<script>
	$(document).ready(function() {  
		var chart = {      
			type: 'pie',
			options3d: {
				enabled: true,
				alpha: 45,
				beta: 0
			}
		};
		var title = {
			text: 'Grafica de casos segun su estado'
		};
		var plotOptions = {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				depth: 35,

				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %'
				}
			}
		};   
		var series = [{
			type: 'pie',
			name: 'Número de casos',
			data: [
			{
				name:'Casos Cerrados',
				y:{{$verifi}},
				color:'#17EC20'
			},
			{
				name:'Casos Abiertos',
				y:{{$solicA}},
				color:'#36A2EB'
			},
			{
				name:'Casos Asignados',
				y:{{$solicB}},
				color:'#FFCD56'
			},
			{
				name:'Casos Pendientes',
				y:{{$solicC}},
				color:'#F88C19'
			},
			{
				name:'Casos en Proceso',
				y:{{$solicD}},
				color:'#AA21FC'
			},
			{
				name:'Casos en Verificación',
				y:{{$solicE}},
				color:'#FF0000'
			},
			]
		}];     
		var json = {};   
		json.chart = chart;
		json.title = title;
		json.plotOptions = plotOptions;
		json.series = series;
		$('#casos').highcharts(json);
	});
</script>
@endsection
