<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<form id="agregarEquipo" method="POST" action="{{route('equipoUpdate',[$equipos->id_Equipo])}}">
	{{ method_field('PUT') }}
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
			<label for="Categoria" class="col-md-12 col-form-label text-md-left">Categoria<span class="red">*</span></label>
			<div class="col-md-12">
				<select class="form-control" id="Categoria" name="Categoria" value="{{$equipos->Categoria}}" disabled="">
					<option value="">Seleccionar</option>
					@foreach($categorias as $cate)
					@if($equipos->Categoria === $cate->id_Categoria)
					<option value="{{$cate->id_Categoria}}" selected="">{{$cate->Nombre}}</option>
					@else
					<option value="{{$cate->id_Categoria}}">{{$cate->Nombre}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-6">
			<label for="Codigo" class="col-md-12 col-form-label text-md-left">Codigo del equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Codigo" type="text" class="form-control" name="Codigo" value="{{$equipos->Codigo}}" autocomplete="Codigo" autofocus placeholder="Ingrese el codigo del equipo" disabled="">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="Modelo" class="col-md-12 col-form-label text-md-left">Modelo del equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Modelo" type="text" class="form-control" name="Modelo" value="{{$equipos->Modelo}}" placeholder="Ingrese el modelo del equipo" autocomplete="Modelo" autofocus disabled="">
			</div>
		</div>
		<div class="col-6">
			<label for="Serie" class="col-md-12 col-form-label text-md-left">Serie del equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Serie" type="text" class="form-control" name="Serie" value="{{$equipos->Serie}}" placeholder="Ingrese la serie del equipo" autocomplete="Serie" autofocus disabled="">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="Marca" class="col-md-12 col-form-label text-md-left">Marca del equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<select class="form-control" id="Marca" name="Marca" value="{{$equipos->Marca}}" disabled="">
					<option value="">Seleccionar</option>
					@foreach($marca as $marcas)
					@if($equipos->Marca === $marcas->id_Marca)
					<option value="{{$marcas->id_Marca}}" selected="">{{$marcas->Marca}}</option>
					@else
					<option value="{{$marcas->id_Marca}}">{{$marcas->Marca}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-6">
			<label for="Tipo" class="col-md-12 col-form-label text-md-left">Tipo de equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<select class="form-control" id="Tipo" name="Tipo" value="{{$equipos->Tipo}}" disabled="">
					<option value="">Seleccionar</option>
					@foreach($tipo as $tipos)
					@if($equipos->Tipo === $tipos->id_Tipo)
					<option value="{{$tipos->id_Tipo}}" selected="">{{$tipos->Tipo}}</option>
					@else
					<option value="{{$tipos->id_Tipo}}">{{$tipos->Tipo}}</option>
					@endif
					@endforeach
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="Dependencia" class="col-md-12 col-form-label text-md-left">Dependencia del equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Dependencia" type="text" class="form-control" name="Dependencia" value="{{$equipos->Dependencia}}" placeholder="Ingrese la dependencia del equipo" autocomplete="Dependencia" autofocus disabled="">
			</div>
		</div>
		<div class="col-6">
			<label for="PSB" class="col-md-12 col-form-label text-md-left">PSB<span class="red">*</span></label>
			<div class="form-check col-6 float-left">
				<div class="form-control">
					<input class="form-check-input check-Ps" type="radio" name="PSB" id="PSB" value="1" @if($equipos->PSB === 1) checked @endif disabled="">
					<label class="form-check-label check-P" for="PSB">
						Si
					</label>
				</div>
			</div>
			<div class="form-check col-6 float-right">
				<div class="form-control">
					<input class="form-check-input check-Ps" type="radio" name="PSB" id="PSB" value="0" @if($equipos->PSB === 0) checked @endif disabled="">
					<label class="form-check-label check-P" for="PSB">
						No
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="Placa" class="col-md-12 col-form-label text-md-left">Placa del equipo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Placa" type="text" class="form-control" name="Placa" value="{{$equipos->Placa}}" placeholder="Ingrese la placa de inventario del equipo" autocomplete="Placa" autofocus disabled="">
			</div>
		</div>
	</div>
</form>