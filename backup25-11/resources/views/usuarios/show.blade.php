<div class="row">
	<div class="col-6">
		<label for="NombreCompleto" class="col-md-12 col-form-label text-md-left">Nombre completo<span class="red">*</span></label>
		<div class="col-md-12">
			<input id="NombreCompleto" type="text" class="form-control" name="NombreCompleto" value="{{$users->NombreCompleto}}" disabled="" autocomplete="NombreCompleto" autofocus placeholder="Ingrese el primer nombre">
		</div>
	</div>
	<div class="col-6">
		<label for="email" class="col-md-12 col-form-label text-md-left">Correo<span class="red">*</span></label>
		<div class="col-md-12">
			<input id="email" type="email" class="form-control" name="email" value="{{$users->email}}" disabled="" placeholder="Ingrese el correo de usuario" autocomplete="email" autofocus>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<label for="Area" class="col-md-12 col-form-label text-md-left">Área Laboral<span class="red">*</span></label>
		<div class="col-md-12">
			<input id="Area" type="text" class="form-control" name="Area" value="{{$users->Area}}" disabled="" placeholder="Ingrese el área del usuario" autocomplete="Area" autofocus>
		</div>
	</div>
	<div class="col-6">
		<label for="Rol" class="col-md-12 col-form-label text-md-left">Rol<span class="red">*</span></label>
		<div class="col-md-12">
			<select class="form-control" id="Rol" name="Rol" value="" disabled="">
				@foreach($rol as $roles)
				@if($roles->id_Rol == $users->Rol)
				<option value="{{$users->Rol}}">{{$roles->Nombre}}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<label for="TipoDoc" class="col-md-12 col-form-label text-md-left">Tipo de Documento<span class="red">*</span></label>
		<div class="col-md-12">
			<select class="form-control" id="TipoDoc" name="TipoDoc" value="" disabled="">
				@foreach($tipoDoc as $tipoDocs)
				@if($users->TipoDoc == $tipoDocs->id_Tipo)
				<option value="{{$users->TipoDoc}}">{{$tipoDocs->Nombre}}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-6">
		<label for="NumeroDoc" class="col-md-12 col-form-label text-md-left">Número de Documento<span class="red">*</span></label>
		<div class="col-md-12">
			<input id="NumeroDoc" type="text" class="form-control" name="NumeroDoc" value="{{$users->NumeroDoc}}" disabled="" placeholder="Ingrese el número de documento" autocomplete="NumeroDoc" autofocus>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<label for="Celular" class="col-md-12 col-form-label text-md-left">Celular<span class="red">*</span></label>
		<div class="col-md-12">
			<input id="Celular" type="text" class="form-control" name="Celular" value="{{$users->Celular}}" disabled="" placeholder="Ingrese el número de celular" autocomplete="Celular" autofocus>
		</div>
	</div>
</div>