<link rel="stylesheet" href="{{asset('css/styleValidate.css')}}">
<form class="ediUser{{$users->id}}" method="POST" action="{{ url('/usuarios/'. $users->id) }}">
	{{ method_field('PUT') }}
	{{ csrf_field() }}
	<div class="row">
		<div class="col-6">
			<label for="NombreCompleto" class="col-md-12 col-form-label text-md-left">Nombre completo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="NombreCompleto" type="text" class="form-control" name="NombreCompleto" value="{{$users->NombreCompleto}}" autocomplete="NombreCompleto" autofocus placeholder="Ingrese el primer nombre">
			</div>
		</div>
		<div class="col-6">
			<label for="email" class="col-md-12 col-form-label text-md-left">Correo<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="email" type="email" class="form-control" name="email" value="{{$users->email}}" placeholder="Ingrese el correo de usuario" autocomplete="email" autofocus>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="password" class="col-md-12 col-form-label text-md-left">Nueva Contraseña<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="password_new{{$users->id}}" type="password" class="form-control" name="password_new" value="{{ old('password') }}" placeholder="Ingrese la contraseña del usuario" autocomplete="password" autofocus>
			</div>
		</div>
		<div class="col-6">
			<label for="password" class="col-md-12 col-form-label text-md-left">Confirmar Contraseña<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Confirme la contraseña" autocomplete="password_confirmation" autofocus>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="Area" class="col-md-12 col-form-label text-md-left">Área Laboral<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Area" type="text" class="form-control" name="Area" value="{{$users->Area}}" placeholder="Ingrese el área del usuario" autocomplete="Area" autofocus>
			</div>
		</div>
		<div class="col-6">
			<label for="Rol" class="col-md-12 col-form-label text-md-left">Rol<span class="red">*</span></label>
			<div class="col-md-12">
				<select class="form-control" id="Rol" name="Rol">
					<option value="1"@if($users->Rol == 1) selected @endif>Supervisor</option>
					<option value="2"@if($users->Rol == 2) selected @endif>Tecnico</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="TipoDoc" class="col-md-12 col-form-label text-md-left">Tipo de Documento<span class="red">*</span></label>
			<div class="col-md-12">
				<select class="form-control" id="TipoDoc" name="TipoDoc" value="">
					<option value="1"@if($users->TipoDoc == 1) selected @endif>CC</option>
					<option value="2"@if($users->TipoDoc == 2) selected @endif>CE</option>
					<option value="3"@if($users->TipoDoc == 3) selected @endif>PA</option>
				</select>
			</div>
		</div>
		<div class="col-6">
			<label for="NumeroDoc" class="col-md-12 col-form-label text-md-left">Número de Documento<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="NumeroDoc" type="text" class="form-control" name="NumeroDoc" value="{{$users->NumeroDoc}}" placeholder="Ingrese el número de documento" autocomplete="NumeroDoc" autofocus>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<label for="Celular" class="col-md-12 col-form-label text-md-left">Celular<span class="red">*</span></label>
			<div class="col-md-12">
				<input id="Celular" type="text" class="form-control" name="Celular" value="{{$users->Celular}}" placeholder="Ingrese el número de celular" autocomplete="Celular" autofocus>
			</div>
		</div>
		<div class="col-6">
			<label for="Estado" class="col-md-12 col-form-label text-md-left">Estado<span class="red">*</span></label>
			<div class="col-md-12">
				<select class="form-control" id="Estado" name="Estado">
					<option value="Habilitado"@if($users->Estado == 'Habilitado') selected @endif>Habilitado</option>
					<option value="Deshabilitado"@if($users->Estado == 'Deshabilitado') selected @endif>Deshabilitado</option>
				</select>
			</div>
		</div>
	</div>
	<div class="modal-footer" style="margin-top: 15px;">
		<a data-dismiss="modal" class="btn btn-close">Cerrar</a>
		<button type="submit" class="btn btn-env">
			Editar
		</button>
	</div>
</form>
