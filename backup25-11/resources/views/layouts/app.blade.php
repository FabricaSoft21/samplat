<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <link rel="icon" type="image/x-icon" href="/img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('SAM') }}</title>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    @include ('sweet::alert')

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    
    <!-- Styles -->
    <link href="{{ asset('css/navStyle.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm" id="bg-info">
            <div class="container" style="margin-right: 50px !important;">
                <a class="navbar-brand textNav" href="{{ url('home') }}">
                    <img src="{{ asset('img/SAM1000X1000.png') }}" alt="..." width="65" class="mr-3 rounded-circle img-thumbnail shadow-sm">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">{{ __('Cancelar') }}</a>
                        </li>
                        @else
                        <li class="nav-item dropdown" style="font-family: 'Lato', sans-serif; font-size: 20px;">
                            <a id="navbarDropdown" class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->email }} <span class="caret"></span>
                                <img src="{{ asset('img/user.svg') }}" class="ml-2" style="width: 2.25rem">
                            </a>

                            <div class="dropdown-menu dropdown-menu-right list-background" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar Sesion') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
          @yield('content')
      </main>
  </div>
  <!-- Vertical navbar -->
  <div class="vertical-nav bg-color" id="sidebar">
    <div class="">
        <div class="media d-flex text-center"><a href="{{ url('home') }}"><img src="{{ asset('img/SAMIcon.png') }}" alt="..." width="70%" ></a>
        </div>
    </div>
    @if (Auth::guest())          
    @else
    <ul class="nav flex-column mb-0">
        @if(Auth::user()->Rol === 1)
        <li class="nav-item">
            <a href="/usuarios" class="nav-link text-white text-center family">
                <img src="{{ asset('img/user.svg') }}" class="fa-size"><br>
                GESTIÓN DE USUARIOS
            </a>
        </li>
        <li class="nav-item">
            <a href="/administracion/index" class="nav-link text-white text-center family">
                <img src="{{ asset('img/edit.svg') }}" class="fa-size"><br>
                ADMINISTRATIVO
            </a>
        </li>
        <li class="nav-item">
            <a href="/equipos/index" class="nav-link text-white text-center family">
                <img src="{{ asset('img/valores.svg') }}" class="fa-size"><br>
                GESTIÓN DE EQUIPOS
            </a>
        </li>
        <li class="nav-item">
            <a href="/mantenimiento" class="nav-link text-white text-center family">
                <img src="{{ asset('img/business-and-finance.svg') }}" class="fa-size"><br>
                GESTIÓN DE SOLICITUDES
            </a>
        </li>
        <li class="nav-item">
            <a href="/dashboard" class="nav-link text-white text-center family">
                <img src="{{ asset('img/tablero.svg') }}" class="fa-size"><br>
                DASHBOARD
            </a>
        </li>
        @elseif(Auth::user()->Rol === 2)
        <li class="nav-item">
            <a href="/mantenimiento" class="nav-link text-white text-center family">
                <img src="{{ asset('img/business-and-finance.svg') }}" class="fa-size"><br>
                GESTIÓN DE SOLICITUDES
            </a>
        </li>
        @endif
    </ul>
    @endif
</div>
<!-- Fin del navbar vertical -->
</body>
</html>
