-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-10-2021 a las 18:50:44
-- Versión del servidor: 5.7.30
-- Versión de PHP: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `samdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_Categoria` bigint(20) UNSIGNED NOT NULL,
  `Nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_Categoria`, `Nombre`) VALUES
(1, 'Aire Acondicionado'),
(2, 'Luminarias'),
(3, 'Otro( Muebles, Vidrios, Ventanas, Puertas, etc)'),
(4, 'Electricidad'),
(5, 'Ascensores'),
(6, 'Luminaria'),
(7, 'Cambio'),
(8, 'Plomeria'),
(9, 'importante probar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edificios`
--

CREATE TABLE `edificios` (
  `id_Edificio` bigint(20) UNSIGNED NOT NULL,
  `Nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `edificios`
--

INSERT INTO `edificios` (`id_Edificio`, `Nombre`) VALUES
(1, 'Torre norte'),
(2, 'Torre sur'),
(3, 'Edificio del agua'),
(4, 'Edificio # 2'),
(5, 'Torre occidente'),
(6, 'Torre de informática'),
(7, 'Torre monitoreo'),
(8, 'Torre mantenimiento'),
(9, 'Torre rectoría'),
(10, 'Torre docentes'),
(11, 'Aula máxima'),
(12, 'polleria'),
(13, 'edificio importante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id_Equipo` bigint(20) UNSIGNED NOT NULL,
  `Codigo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Modelo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Serie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dependencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Placa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PSB` tinyint(1) NOT NULL,
  `Categoria` bigint(20) UNSIGNED NOT NULL,
  `Marca` bigint(20) UNSIGNED NOT NULL,
  `Tipo` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id_Equipo`, `Codigo`, `Modelo`, `Serie`, `Dependencia`, `Placa`, `PSB`, `Categoria`, `Marca`, `Tipo`) VALUES
(1, '989', '2014nexu', '456y78', 'Norte', 'lopo08jkj', 1, 2, 1, 2),
(2, '9', '6785hy7este lo huce yer', '2uy7878', ',,,,ojhytghoo', 'lop509ptj', 0, 1, 2, 2),
(3, '121234', '6785hy7este lo huce yer', 'nsnsnwuw528818363', '729wngd', '123123', 1, 8, 1, 1),
(4, '009024', 'no se', '01340114002', 'no se', 'd465d48d46', 1, 4, 2, 1),
(5, '32323232', 'sssssssssssssssss', 'eewwewewewed<dsdd', '2222222222222', 'wwwwwwwwwwwwwwwwwwwwa', 0, 8, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `espacios`
--

CREATE TABLE `espacios` (
  `id_Espacio` bigint(20) UNSIGNED NOT NULL,
  `Espacio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `espacios`
--

INSERT INTO `espacios` (`id_Espacio`, `Espacio`) VALUES
(1, 'Ambiente 101'),
(2, 'Ambiente 102'),
(3, 'Ambiente 103'),
(4, 'Ambiente 104'),
(5, 'Ambiente 105'),
(6, 'Ambiente 201'),
(7, 'Ambiente 202'),
(8, 'Ambiente 203'),
(9, 'Ambiente 204'),
(10, 'Ambiente 205'),
(11, 'Ambiente 301'),
(12, 'Ambiente 302'),
(13, 'Ambiente 303'),
(14, 'Ambiente 304'),
(15, 'Ambiente 305'),
(16, 'Ambiente 401'),
(17, 'Ambiente 402'),
(18, 'Ambiente 403'),
(19, 'Ambiente 404'),
(20, 'Ambiente 405'),
(21, 'Ambiente 501'),
(22, 'Ambiente 502'),
(23, 'Ambiente 503'),
(24, 'Ambiente 504'),
(25, 'Ambiente 505'),
(26, 'Ambiente 502');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha_mantenimientos`
--

CREATE TABLE `ficha_mantenimientos` (
  `id_Ficha` bigint(20) UNSIGNED NOT NULL,
  `TipoMante` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Observacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_Solicitud` bigint(20) UNSIGNED NOT NULL,
  `ImagenSoli` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ficha_mantenimientos`
--

INSERT INTO `ficha_mantenimientos` (`id_Ficha`, `TipoMante`, `Observacion`, `id_Solicitud`, `ImagenSoli`) VALUES
(1, 'Correctivo', 'Se cambiaron las 8 lamparas, ya que 6 estaban quemadas y 2 tenían los contactos fundidos', 1, 1),
(2, 'Correctivo', 'Se cambiaron 4 fusibles, sin embargo la caja necesita 6 fusibles para funcionar correctamente', 2, 2),
(3, 'Correctivo', 'No se pudo realizar el trabajo ya que no se tenían los tornillos de la medida correcta para fijar la chapa', 3, 2),
(4, 'Correctivo', 'Se cambio 1 fusible', 5, 3),
(5, 'Correctivo', 'Cuack cuack', 11, 3),
(6, 'Preventivo', 'siuuuu', 9, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `id_Imagen` bigint(20) UNSIGNED NOT NULL,
  `Imagen_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`id_Imagen`, `Imagen_1`, `Imagen_2`, `Imagen_3`, `Imagen_4`, `Imagen_5`) VALUES
(1, '1595872495clasesinluz.jpg', '1595872495clasessinluz.jpg', '1595872495clases-sin-luz.jpg', NULL, NULL),
(2, '1595872795caja_fusible2.png', '1595872795caja_fusibles5.jpg', NULL, NULL, NULL),
(3, '1595872929chapas_rotas.jpg', '1595872929como-repara.jpg', '1595872929CÓRDOBA5.jpg', '1595872929image-967270.jpg', '1595872930unnamed.jpg'),
(4, NULL, NULL, NULL, NULL, NULL),
(5, '1598449040clasesinluz.jpg', '1598449040clasessinluz.jpg', '1598449040clases-sin-luz.jpg', NULL, NULL),
(6, '16020226512.jpg', '160202265144.jpg', '1602022651Captura1.PNG', '1602022651descarga 2.jpg', '1602022651descarga.jpg'),
(7, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL),
(11, '1604515255100365_lalM96nQ.png', '1604515255soul1.jpg', NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL),
(17, NULL, NULL, NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, NULL),
(19, NULL, NULL, NULL, NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL),
(21, NULL, NULL, NULL, NULL, NULL),
(22, NULL, NULL, NULL, NULL, NULL),
(23, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL),
(25, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL),
(31, NULL, NULL, NULL, NULL, NULL),
(32, NULL, NULL, NULL, NULL, NULL),
(33, NULL, NULL, NULL, NULL, NULL),
(34, NULL, NULL, NULL, NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `img_mantenimientos`
--

CREATE TABLE `img_mantenimientos` (
  `id_Img` bigint(20) UNSIGNED NOT NULL,
  `Imagen_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Imagen_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `img_mantenimientos`
--

INSERT INTO `img_mantenimientos` (`id_Img`, `Imagen_1`, `Imagen_2`, `Imagen_3`, `Imagen_4`, `Imagen_5`) VALUES
(1, '1595873834classLuz1.jpg', '1595873834classLuz2.jpg', '1595873834classLuz3.jpg', NULL, NULL),
(2, '1595874040caja_fusibles.jpg', '1595874040caja_fusibles3.jpg', '1595874040caja_fusibles4.jpg', NULL, NULL),
(3, '1599059094caja_fusible2.png', '1599059094caja_fusibles4.jpg', '1599059094caja_fusibles5.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos`
--

CREATE TABLE `insumos` (
  `id_Insumo` bigint(20) UNSIGNED NOT NULL,
  `Insumo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `insumos`
--

INSERT INTO `insumos` (`id_Insumo`, `Insumo`, `Cantidad`) VALUES
(1, 'Taladro', 3),
(2, 'Caja tornillos', 933),
(3, 'Rollo cable', 582),
(4, 'Cinta aislante', 1065),
(5, 'Alicate', 572),
(6, 'Lampara', 1681),
(7, 'Fusibles', 889),
(8, 'Chapas', 531),
(9, 'locas', 45);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumos_sobrantes`
--

CREATE TABLE `insumos_sobrantes` (
  `id_Insumo_Sobrante` bigint(20) UNSIGNED NOT NULL,
  `id_Ficha` bigint(20) UNSIGNED NOT NULL,
  `id_Insumo` bigint(20) UNSIGNED NOT NULL,
  `CantidadSobrante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `insumos_sobrantes`
--

INSERT INTO `insumos_sobrantes` (`id_Insumo_Sobrante`, `id_Ficha`, `id_Insumo`, `CantidadSobrante`) VALUES
(1, 1, 1, 1),
(2, 1, 6, 0),
(3, 2, 7, 0),
(4, 2, 5, 1),
(5, 2, 2, 0),
(6, 3, 8, 1),
(7, 4, 7, 1),
(8, 5, 1, 1),
(9, 5, 3, 2),
(10, 6, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumo_solicituds`
--

CREATE TABLE `insumo_solicituds` (
  `id_Insumo_Solicitud` bigint(20) UNSIGNED NOT NULL,
  `id_Solicitud` bigint(20) UNSIGNED NOT NULL,
  `id_Insumo` bigint(20) UNSIGNED NOT NULL,
  `CantidadUsada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `insumo_solicituds`
--

INSERT INTO `insumo_solicituds` (`id_Insumo_Solicitud`, `id_Solicitud`, `id_Insumo`, `CantidadUsada`) VALUES
(1, 1, 1, 1),
(2, 1, 6, 8),
(3, 2, 7, 4),
(4, 2, 5, 1),
(5, 2, 2, 1),
(6, 3, 8, 1),
(7, 4, 1, 1),
(8, 5, 7, 2),
(9, 11, 1, 4),
(10, 11, 3, 3),
(11, 9, 1, 758);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id_Marca` bigint(20) UNSIGNED NOT NULL,
  `Marca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id_Marca`, `Marca`) VALUES
(1, 'Dell'),
(2, 'Kingston'),
(3, 'Samsung'),
(4, 'Hp'),
(5, 'asus'),
(6, 'SENA'),
(7, 'EWEWEWE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(39, '2013_03_26_160342_create_tipo_documento_table', 1),
(40, '2013_03_26_170256_create_roles_table', 1),
(41, '2014_10_12_000000_create_users_table', 1),
(42, '2014_10_12_100000_create_password_resets_table', 1),
(43, '2020_03_30_195222_create_categorias_table', 1),
(44, '2020_03_31_152430_create_edificios_table', 1),
(45, '2020_03_31_154814_create_pisos_table', 1),
(46, '2020_03_31_162923_create_imagenes_table', 1),
(47, '2020_03_31_163206_create_espacios_table', 1),
(48, '2020_03_31_170743_create_prioridades_table', 1),
(49, '2020_03_31_170838_create_insumos_table', 1),
(50, '2020_03_31_170840_create_solicitud_mantenimientos_table', 1),
(51, '2020_04_29_164024_create_marcas_table', 1),
(52, '2020_04_29_164131_create_tipos_table', 1),
(53, '2020_04_29_422401_create_equipos_table', 1),
(54, '2020_05_14_114359_create_insumo_solicituds_table', 1),
(55, '2020_05_28_105233_create_img_mantenimientos_table', 1),
(56, '2020_05_28_105234_create_ficha_mantenimientos_table', 1),
(57, '2020_06_03_120203_create_insumos_sobrantes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `id_Piso` bigint(20) UNSIGNED NOT NULL,
  `Piso` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`id_Piso`, `Piso`) VALUES
(1, 'Piso 1'),
(2, 'Piso 2'),
(3, 'Piso 3'),
(4, 'Piso 4'),
(5, 'Piso 5'),
(6, 'Piso 6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prioridades`
--

CREATE TABLE `prioridades` (
  `id_Prioridad` bigint(20) UNSIGNED NOT NULL,
  `Prioridad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tiempo` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `prioridades`
--

INSERT INTO `prioridades` (`id_Prioridad`, `Prioridad`, `Tiempo`) VALUES
(1, 'Alta', 31),
(2, 'Media', 48),
(3, 'Baja', 72);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_Rol` bigint(20) UNSIGNED NOT NULL,
  `Nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_Rol`, `Nombre`) VALUES
(1, 'Supervisor'),
(2, 'Tecnico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_mantenimientos`
--

CREATE TABLE `solicitud_mantenimientos` (
  `id_Solicitud` bigint(20) UNSIGNED NOT NULL,
  `NombreCompleto` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Prioridad` bigint(20) UNSIGNED NOT NULL,
  `Categoria` bigint(20) UNSIGNED NOT NULL,
  `Edificio` bigint(20) UNSIGNED NOT NULL,
  `Piso` bigint(20) UNSIGNED NOT NULL,
  `Espacio` bigint(20) UNSIGNED NOT NULL,
  `Descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `InsumoAsignado` tinyint(1) NOT NULL DEFAULT '0',
  `Imagen` bigint(20) UNSIGNED NOT NULL,
  `Estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Abierto',
  `Tecnico` bigint(20) UNSIGNED DEFAULT NULL,
  `Fecha_solicitud` datetime NOT NULL,
  `Fecha_fin` datetime NOT NULL,
  `Fecha_asignacion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `solicitud_mantenimientos`
--

INSERT INTO `solicitud_mantenimientos` (`id_Solicitud`, `NombreCompleto`, `Email`, `Prioridad`, `Categoria`, `Edificio`, `Piso`, `Espacio`, `Descripcion`, `InsumoAsignado`, `Imagen`, `Estado`, `Tecnico`, `Fecha_solicitud`, `Fecha_fin`, `Fecha_asignacion`) VALUES
(1, 'Yojan Stiven Zapata Garcia', 'yojan32zaz@gmail.com', 1, 2, 1, 3, 14, 'Las lamparas del ambiente se quemaron y el ambiente se encuentra en total oscuridad', 1, 1, 'Verificado', 2, '2020-07-27 12:54:55', '2020-07-28 12:54:55', '2020-07-28 12:57:51'),
(2, 'Ricardo milos', 'yojan32zaz@gmail.com', 2, 4, 2, 1, 4, 'La caja de fusiles tubo un corto y uno de los fusibles se fundio', 1, 2, 'Verificado', 2, '2020-07-27 12:59:55', '2020-07-29 12:59:55', '2020-07-29 13:03:02'),
(3, 'Dhiana montes', 'yojan32zaz@gmail.com', 3, 3, 1, 2, 10, 'Rompieron la chapa de la puerta del ambiente', 1, 3, 'Verificado', 2, '2020-07-27 13:02:10', '2020-07-30 13:02:10', '2020-07-30 13:03:24'),
(4, 'Yojan Stiven Zapata Garcia', 'yojan32zaz@gmail.com', 3, 2, 1, 3, 14, 'Las luces dejaron de encender', 1, 4, 'Asignado', 2, '2020-08-13 13:35:52', '2020-08-16 13:35:52', '2020-08-16 13:39:25'),
(5, 'Yojan Stiven Zapata Garcia', 'yojan32zaz@gmail.com', 2, 4, 1, 3, 14, 'El piso esta a oscuras', 1, 5, 'Verificado', 2, '2020-08-26 08:37:21', '2020-08-28 08:37:21', '2020-09-04 10:01:02'),
(6, 'Liseth', 'pruebapez@yopmail.com', 1, 2, 2, 3, 15, 'AHUHUD hasuwh.,900nwhdishs', 0, 6, 'Asignado', 8, '2020-10-06 17:17:31', '2020-10-07 17:17:31', '2020-10-21 21:23:33'),
(7, 'Liseth', 'pruebapez@yopmail.com', 1, 2, 1, 3, 14, 'aaas5fghhhhhhhhhh', 0, 7, 'Asignado', 14, '2020-10-08 09:51:28', '2020-10-09 09:51:28', '2020-10-21 21:31:14'),
(8, 'Santiago tamal', 'santiagovelez345@gmail.com', 2, 1, 2, 3, 12, 'Mk se me apago el hpta de aire', 0, 8, 'Asignado', 8, '2020-10-20 14:04:19', '2020-10-22 14:04:19', '2020-10-22 14:11:29'),
(9, 'Pollo Agustin', 'santivc345@gmail.com', 3, 1, 2, 3, 14, 'Siuuuuuu', 1, 9, 'Verificado', 8, '2020-11-04 12:36:29', '2020-11-07 12:36:29', '2020-11-07 12:37:24'),
(10, 'Jijijuputa', 'santivc345@gmail.com', 2, 2, 9, 2, 16, 'siuuuuuuu el bicho', 0, 10, 'Asignado', 8, '2020-11-04 13:37:41', '2020-11-06 13:37:41', '2020-11-06 13:38:37'),
(11, 'Bartender', 'santivc345@gmail.com', 1, 5, 5, 1, 23, 'Se me dañaron las luces, ahhhhhh', 1, 11, 'Verificado', 8, '2020-11-04 13:40:55', '2020-11-05 20:40:55', '2020-11-05 20:41:16'),
(12, '2312', 'santiagovelez345@gmail.com', 3, 1, 11, 4, 15, 'deddfdf', 0, 12, 'Asignado', 8, '2020-12-01 09:57:36', '2020-12-04 09:57:36', '2020-12-21 14:15:59'),
(13, '33232434', 'santiagovelez345@gmail.com', 2, 3, 10, 2, 16, 'sdffdkff', 0, 13, 'Asignado', 11, '2020-12-01 09:58:44', '2020-12-03 09:58:44', '2021-10-06 08:22:41'),
(14, 'Jhony', 'santiagovelez345@gmail.com', 1, 5, 5, 3, 14, 'Uy marica que cayo el mk ascensor jsjas', 0, 14, 'Asignado', 8, '2020-12-22 11:09:30', '2020-12-23 18:09:30', '2021-10-05 15:27:00'),
(15, 'Karelys', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 15, 'Asignado', 2, '2020-12-22 16:17:15', '2020-12-23 23:17:15', '2021-10-01 16:49:52'),
(16, 'Suso Ruperto', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 16, 'Asignado', 9, '2020-12-22 16:24:48', '2020-12-23 23:24:48', '2021-10-01 18:24:34'),
(17, 'Uribe', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 17, 'Asignado', 9, '2020-12-22 16:32:04', '2020-12-23 23:32:04', '2021-10-05 15:27:20'),
(18, 'Uribe', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 19, 'Asignado', 2, '2021-02-17 15:56:29', '2021-02-18 22:56:29', '2021-10-05 15:29:04'),
(19, 'Uribe', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 20, 'Asignado', 14, '2021-02-17 16:01:12', '2021-02-18 23:01:12', '2021-10-05 15:29:20'),
(20, 'Uribe', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 21, 'Asignado', 8, '2021-02-17 16:12:14', '2021-02-18 23:12:14', '2021-10-05 15:30:32'),
(21, 'Uribe', 'karelys@gmail.co', 1, 2, 1, 4, 1, 'La señorita Karelys dañó un TV', 0, 22, 'Asignado', 2, '2021-02-17 16:44:59', '2021-02-18 23:44:59', '2021-10-05 15:27:59'),
(22, 'karre', 'kare@gmail.com', 1, 5, 5, 5, 17, 'mantenimiento habitual', 0, 24, 'Asignado', 11, '2021-03-01 16:04:51', '2021-03-02 23:04:51', '2021-10-05 15:19:03'),
(23, 'maria isabel triviño borjas', 'triviborjas@gmail.com', 1, 4, 1, 4, 8, 'no sube al piso 4', 0, 25, 'Asignado', 8, '2021-09-29 10:45:04', '2021-09-30 17:45:04', '2021-09-30 17:46:21'),
(24, 'maria isabel triviño borjas dps', 'triviborjas@gmail.com', 1, 4, 4, 5, 19, 'MEQUEDE DIN LUZ', 0, 27, 'Asignado', 11, '2021-09-30 09:12:39', '2021-10-01 16:12:39', '2021-10-05 15:28:40'),
(25, 'Lizeth Lopez', 'Lialopezr@sena.edu.co', 1, 5, 2, 3, 15, 'El ascensor no abre en el piso 3.', 0, 34, 'Abierto', NULL, '2021-10-04 08:40:07', '2021-10-05 15:40:07', NULL),
(26, 'Any Caicedo', 'aycaicedo@sena.edu.co', 1, 6, 1, 3, 13, 'Las lamparas no encienden', 0, 35, 'Abierto', NULL, '2021-10-04 08:43:35', '2021-10-05 15:43:35', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos`
--

CREATE TABLE `tipos` (
  `id_Tipo` bigint(20) UNSIGNED NOT NULL,
  `Tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipos`
--

INSERT INTO `tipos` (`id_Tipo`, `Tipo`) VALUES
(1, 'Eléctrico'),
(2, 'Hidroeléctrico'),
(3, 'colagranulada mia'),
(4, 'DDDDDDDD');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id_Tipo` bigint(20) UNSIGNED NOT NULL,
  `Nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id_Tipo`, `Nombre`) VALUES
(1, 'CC'),
(2, 'CE'),
(3, 'PA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Area` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NombreCompleto` varchar(170) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TipoDoc` bigint(20) UNSIGNED NOT NULL,
  `NumeroDoc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Estado` enum('Habilitado','Deshabilitado') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Habilitado',
  `Rol` bigint(20) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `Area`, `NombreCompleto`, `TipoDoc`, `NumeroDoc`, `Celular`, `Estado`, `Rol`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'pruebasoft@misena.edu.co', '$2a$12$j4kfsORUocnq7W5B.6dfleQo/cEGdlBOxuMxZeqmsUb1tgXBVKpuu', 'Administración', 'Pruebas', 1, '0123456789', '9876543210', 'Habilitado', 2, NULL, NULL, '2021-10-04 06:35:55'),
(2, 'yojan32zaz@gmail.com', '$2y$10$a19d7mS9TP1KHrMiDUr/yOILCJyOfNC.siput1VFm0bGBOafFulLW', 'Mantenimiento', 'Yojan stiven zapata', 1, '1000088922', '3118347937', 'Habilitado', 2, NULL, '2020-07-27 10:03:30', '2020-07-27 10:34:52'),
(7, 'prueba1@yopmail.com', '$2y$10$a19d7mS9TP1KHrMiDUr/yOILCJyOfNC.siput1VFm0bGBOafFulLW', 'Administracion', 'Aguacate', 1, '1192772704', '3013889664', 'Habilitado', 1, NULL, NULL, NULL),
(8, 'prueba2@yopmail.com', '$2y$10$a19d7mS9TP1KHrMiDUr/yOILCJyOfNC.siput1VFm0bGBOafFulLW', 'Mantenimmiento', 'Pollo brasas', 1, '12981823991', '231029923', 'Deshabilitado', 2, NULL, NULL, '2021-09-29 08:50:00'),
(9, 'pruebapez@yopmail.com', '$2y$10$dpAr15C8y8sLGefdjNNkqOW175B/Y6ylx8zyOihc6uadrCZFEil9G', 'defensa', 'Andrea', 1, '4520093', '4536789', 'Deshabilitado', 2, NULL, '2020-10-14 09:19:06', '2020-10-16 09:33:14'),
(10, 'santivc345@yopmail.com', '$2y$10$bwfNqOv2.xSFTLftEWzZT.Xd2NPpzajfT2PhAQoMwwWmlZdNO/kqW', 'Tecnico', 'Pollo Hernesto', 1, '1', '123', 'Habilitado', 1, NULL, '2020-10-14 11:54:51', '2020-10-14 12:02:37'),
(11, 'tenico@gmail.com', '$2y$10$Ivy6tXzRLU6HYa.V6YQhAut//PxoFe0S0I9T5pJa/.QiU0XBkEqAa', 'electrico', 'TECNICO', 1, '2345667788', '33444', 'Habilitado', 2, NULL, '2020-10-16 14:19:18', '2020-10-16 14:19:18'),
(12, 'andreahyj@gmail.com', '$2y$10$IegODk5WpVMdTGrVZqELZOaLjjmPYXjbAqeyQU7X0ydsMRLfW/.o6', 'aaaa', 'Andrea', 1, '123221111', '22333', 'Habilitado', 1, NULL, '2020-10-16 14:20:35', '2020-10-16 14:20:35'),
(13, 'administrador@gmail.com', '$2y$10$G7D32l0gH4n4iwe2kaWhy.6qG3dyMVzs6sxLnwhQwEcpczl0SceeG', 'ddddddddddd', 'swedf', 1, '33344', '333333', 'Habilitado', 1, NULL, '2020-10-16 14:22:18', '2020-10-16 14:22:18'),
(14, 'loaL@GMAIL.COM', '$2y$10$2oRhS8Knfyh9J4vEQbh8XOZWE1jLS35IL9svXPLs9ALNP0GGYQy5u', 'aaaaaaaaaa', 'tueres', 1, '1111111111', '2222222222222222', 'Habilitado', 2, NULL, '2020-10-16 14:24:35', '2020-10-16 14:24:35'),
(15, 'jsjs@hotmail.com', '$2y$10$wohZnEshKGPQkqFhTX1f.O6U0N3ob0Ktg3mn0nT/4cOAOkPwfhnUe', 'sss', 'edilms', 1, '999888777777', '7777777777777777777777777777777777', 'Habilitado', 2, NULL, '2020-10-16 14:26:06', '2020-10-16 14:26:06'),
(17, 'levuxupuji@mailinator.com', '$2y$10$m0cSs9/qbGxYrVYpC7m/QuczP4utIudFfthXW4Qn4PJU1b7h1nbB6', 'Ab est recusandae', 'Exercitation omnis q', 2, 'Est repellendus Qu', 'Sed minim aliquam qu', 'Habilitado', 2, NULL, '2021-09-29 08:52:07', '2021-09-29 08:52:07'),
(18, 'maria.trivino7962@miremington.edu.co', '$2y$10$LDNBoV70wGdS1S8gJgAELOBY4JAR0VFN2qm.g8IO1fyfn5KPzpfSu', 'admin', 'jesusa lopez', 1, '1890796378', '3108979865', 'Habilitado', 1, NULL, '2021-09-30 07:17:04', '2021-09-30 07:18:09'),
(19, 'bumemoxunu@mailinator.com', '$2y$10$pxcTUoQLGPpFJvAPfc15.etWXp0yw5TT.1n6kVSdkgM6L2SF7Zqfe', 'Nisi voluptas culpa', 'Quidem quae id dolo', 3, '232323232323', '3456343434', 'Habilitado', 2, NULL, '2021-10-01 10:57:55', '2021-10-01 10:57:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_Categoria`);

--
-- Indices de la tabla `edificios`
--
ALTER TABLE `edificios`
  ADD PRIMARY KEY (`id_Edificio`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id_Equipo`),
  ADD KEY `equipos_categoria_foreign` (`Categoria`),
  ADD KEY `equipos_marca_foreign` (`Marca`),
  ADD KEY `equipos_tipo_foreign` (`Tipo`);

--
-- Indices de la tabla `espacios`
--
ALTER TABLE `espacios`
  ADD PRIMARY KEY (`id_Espacio`);

--
-- Indices de la tabla `ficha_mantenimientos`
--
ALTER TABLE `ficha_mantenimientos`
  ADD PRIMARY KEY (`id_Ficha`),
  ADD KEY `ficha_mantenimientos_id_solicitud_foreign` (`id_Solicitud`),
  ADD KEY `ficha_mantenimientos_imagensoli_foreign` (`ImagenSoli`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id_Imagen`);

--
-- Indices de la tabla `img_mantenimientos`
--
ALTER TABLE `img_mantenimientos`
  ADD PRIMARY KEY (`id_Img`);

--
-- Indices de la tabla `insumos`
--
ALTER TABLE `insumos`
  ADD PRIMARY KEY (`id_Insumo`);

--
-- Indices de la tabla `insumos_sobrantes`
--
ALTER TABLE `insumos_sobrantes`
  ADD PRIMARY KEY (`id_Insumo_Sobrante`),
  ADD KEY `insumos_sobrantes_id_ficha_foreign` (`id_Ficha`),
  ADD KEY `insumos_sobrantes_id_insumo_foreign` (`id_Insumo`);

--
-- Indices de la tabla `insumo_solicituds`
--
ALTER TABLE `insumo_solicituds`
  ADD PRIMARY KEY (`id_Insumo_Solicitud`),
  ADD KEY `insumo_solicituds_id_solicitud_foreign` (`id_Solicitud`),
  ADD KEY `insumo_solicituds_id_insumo_foreign` (`id_Insumo`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id_Marca`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`id_Piso`);

--
-- Indices de la tabla `prioridades`
--
ALTER TABLE `prioridades`
  ADD PRIMARY KEY (`id_Prioridad`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_Rol`);

--
-- Indices de la tabla `solicitud_mantenimientos`
--
ALTER TABLE `solicitud_mantenimientos`
  ADD PRIMARY KEY (`id_Solicitud`),
  ADD KEY `solicitud_mantenimientos_prioridad_foreign` (`Prioridad`),
  ADD KEY `solicitud_mantenimientos_categoria_foreign` (`Categoria`),
  ADD KEY `solicitud_mantenimientos_edificio_foreign` (`Edificio`),
  ADD KEY `solicitud_mantenimientos_piso_foreign` (`Piso`),
  ADD KEY `solicitud_mantenimientos_espacio_foreign` (`Espacio`),
  ADD KEY `solicitud_mantenimientos_imagen_foreign` (`Imagen`),
  ADD KEY `solicitud_mantenimientos_tecnico_foreign` (`Tecnico`);

--
-- Indices de la tabla `tipos`
--
ALTER TABLE `tipos`
  ADD PRIMARY KEY (`id_Tipo`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id_Tipo`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_tipodoc_foreign` (`TipoDoc`),
  ADD KEY `users_rol_foreign` (`Rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_Categoria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `edificios`
--
ALTER TABLE `edificios`
  MODIFY `id_Edificio` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id_Equipo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `espacios`
--
ALTER TABLE `espacios`
  MODIFY `id_Espacio` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `ficha_mantenimientos`
--
ALTER TABLE `ficha_mantenimientos`
  MODIFY `id_Ficha` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id_Imagen` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `img_mantenimientos`
--
ALTER TABLE `img_mantenimientos`
  MODIFY `id_Img` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `insumos`
--
ALTER TABLE `insumos`
  MODIFY `id_Insumo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `insumos_sobrantes`
--
ALTER TABLE `insumos_sobrantes`
  MODIFY `id_Insumo_Sobrante` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `insumo_solicituds`
--
ALTER TABLE `insumo_solicituds`
  MODIFY `id_Insumo_Solicitud` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id_Marca` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `id_Piso` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `prioridades`
--
ALTER TABLE `prioridades`
  MODIFY `id_Prioridad` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_Rol` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `solicitud_mantenimientos`
--
ALTER TABLE `solicitud_mantenimientos`
  MODIFY `id_Solicitud` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `tipos`
--
ALTER TABLE `tipos`
  MODIFY `id_Tipo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id_Tipo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `equipos_categoria_foreign` FOREIGN KEY (`Categoria`) REFERENCES `categorias` (`id_Categoria`),
  ADD CONSTRAINT `equipos_marca_foreign` FOREIGN KEY (`Marca`) REFERENCES `marcas` (`id_Marca`),
  ADD CONSTRAINT `equipos_tipo_foreign` FOREIGN KEY (`Tipo`) REFERENCES `tipos` (`id_Tipo`);

--
-- Filtros para la tabla `ficha_mantenimientos`
--
ALTER TABLE `ficha_mantenimientos`
  ADD CONSTRAINT `ficha_mantenimientos_id_solicitud_foreign` FOREIGN KEY (`id_Solicitud`) REFERENCES `solicitud_mantenimientos` (`id_Solicitud`),
  ADD CONSTRAINT `ficha_mantenimientos_imagensoli_foreign` FOREIGN KEY (`ImagenSoli`) REFERENCES `img_mantenimientos` (`id_Img`);

--
-- Filtros para la tabla `insumos_sobrantes`
--
ALTER TABLE `insumos_sobrantes`
  ADD CONSTRAINT `insumos_sobrantes_id_ficha_foreign` FOREIGN KEY (`id_Ficha`) REFERENCES `ficha_mantenimientos` (`id_Ficha`),
  ADD CONSTRAINT `insumos_sobrantes_id_insumo_foreign` FOREIGN KEY (`id_Insumo`) REFERENCES `insumos` (`id_Insumo`);

--
-- Filtros para la tabla `insumo_solicituds`
--
ALTER TABLE `insumo_solicituds`
  ADD CONSTRAINT `insumo_solicituds_id_insumo_foreign` FOREIGN KEY (`id_Insumo`) REFERENCES `insumos` (`id_Insumo`),
  ADD CONSTRAINT `insumo_solicituds_id_solicitud_foreign` FOREIGN KEY (`id_Solicitud`) REFERENCES `solicitud_mantenimientos` (`id_Solicitud`);

--
-- Filtros para la tabla `solicitud_mantenimientos`
--
ALTER TABLE `solicitud_mantenimientos`
  ADD CONSTRAINT `solicitud_mantenimientos_categoria_foreign` FOREIGN KEY (`Categoria`) REFERENCES `categorias` (`id_Categoria`),
  ADD CONSTRAINT `solicitud_mantenimientos_edificio_foreign` FOREIGN KEY (`Edificio`) REFERENCES `edificios` (`id_Edificio`),
  ADD CONSTRAINT `solicitud_mantenimientos_espacio_foreign` FOREIGN KEY (`Espacio`) REFERENCES `espacios` (`id_Espacio`),
  ADD CONSTRAINT `solicitud_mantenimientos_imagen_foreign` FOREIGN KEY (`Imagen`) REFERENCES `imagenes` (`id_Imagen`),
  ADD CONSTRAINT `solicitud_mantenimientos_piso_foreign` FOREIGN KEY (`Piso`) REFERENCES `pisos` (`id_Piso`),
  ADD CONSTRAINT `solicitud_mantenimientos_prioridad_foreign` FOREIGN KEY (`Prioridad`) REFERENCES `prioridades` (`id_Prioridad`),
  ADD CONSTRAINT `solicitud_mantenimientos_tecnico_foreign` FOREIGN KEY (`Tecnico`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_rol_foreign` FOREIGN KEY (`Rol`) REFERENCES `roles` (`id_Rol`),
  ADD CONSTRAINT `users_tipodoc_foreign` FOREIGN KEY (`TipoDoc`) REFERENCES `tipo_documento` (`id_Tipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
