<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudMantenimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_mantenimientos', function (Blueprint $table) {
            $table->id('id_Solicitud');
            $table->string('NombreCompleto', 170);
            $table->string('Email');
            $table->unsignedBigInteger('Prioridad');
            $table->foreign('Prioridad')->references('id_Prioridad')->on('prioridades');
            $table->unsignedBigInteger('Categoria');
            $table->foreign('Categoria')->references('id_Categoria')->on('categorias');
            $table->unsignedBigInteger('Edificio');
            $table->foreign('Edificio')->references('id_Edificio')->on('edificios');
            $table->unsignedBigInteger('Piso');
            $table->foreign('Piso')->references('id_Piso')->on('pisos');
            $table->unsignedBigInteger('Espacio');
            $table->foreign('Espacio')->references('id_Espacio')->on('espacios');
            $table->string('Descripcion');
            $table->boolean('InsumoAsignado')->default(0);
            $table->unsignedBigInteger('Imagen');
            $table->foreign('Imagen')->references('id_Imagen')->on('imagenes');
            $table->string('Estado')->default('Abierto');
            $table->unsignedBigInteger('Tecnico')->nullable();
            $table->foreign('Tecnico')->references('id')->on('users');
            $table->datetime('Fecha_solicitud');
            $table->datetime('Fecha_fin');
            $table->datetime('Fecha_asignacion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_mantenimientos');
    }
}
