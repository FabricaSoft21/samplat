<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('Area', 120);
            $table->string('NombreCompleto', 170);
            $table->unsignedBigInteger('TipoDoc');
            $table->foreign('TipoDoc')->references('id_Tipo')->on('tipo_documento');
            $table->string('NumeroDoc');
            $table->string('Celular');
            $table->enum('Estado',['Habilitado','Deshabilitado'])->default('Habilitado');
            $table->unsignedBigInteger('Rol');
            $table->foreign('Rol')->references('id_Rol')->on('roles');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
