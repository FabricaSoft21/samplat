<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsumosSobrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insumos_sobrantes', function (Blueprint $table) {
            $table->id('id_Insumo_Sobrante');
            $table->unsignedBigInteger('id_Ficha');
            $table->foreign('id_Ficha')->references('id_Ficha')->on('ficha_mantenimientos');
            $table->unsignedBigInteger('id_Insumo');
            $table->foreign('id_Insumo')->references('id_Insumo')->on('insumos');
            $table->integer('CantidadSobrante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insumos_sobrantes');
    }
}
