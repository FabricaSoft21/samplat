<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsumoSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insumo_solicituds', function (Blueprint $table) {
            $table->id('id_Insumo_Solicitud');
            $table->unsignedBigInteger('id_Solicitud');
            $table->foreign('id_Solicitud')->references('id_Solicitud')->on('solicitud_mantenimientos');
            $table->unsignedBigInteger('id_Insumo');
            $table->foreign('id_Insumo')->references('id_Insumo')->on('insumos');
            $table->integer('CantidadUsada');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insumo_solicituds');
    }
}
