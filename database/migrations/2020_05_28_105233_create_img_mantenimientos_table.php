<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImgMantenimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('img_mantenimientos', function (Blueprint $table) {
            $table->id('id_Img');
            $table->string('Imagen_1');
            $table->string('Imagen_2');
            $table->string('Imagen_3');
            $table->string('Imagen_4');
            $table->string('Imagen_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('img_mantenimientos');
    }
}
