<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    		'email' => 'pruebasoft@misena.edu.co',
    		'password' => bcrypt('123123'),
    		'Area' => 'Administracion',
    		'NombreCompleto' => 'Pruebas',
    		'TipoDoc' => '1',
    		'NumeroDoc' => '0123456789',
    		'Celular' => '9876543210',
    		'Estado' => 'Habilitado',
    		'Rol' => '1',
    	]);
    }
}
