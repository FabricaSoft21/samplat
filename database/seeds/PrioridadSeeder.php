<?php

use Illuminate\Database\Seeder;

class PrioridadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prioridades')->insert([
    		'Prioridad' => 'Alta',
    	]);
    	DB::table('prioridades')->insert([
    		'Prioridad' => 'Media',
    	]);
    	DB::table('prioridades')->insert([
    		'Prioridad' => 'Baja',
    	]);
    }
}
