<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\prioridad;
use App\Models\solicitudMantenimiento;

class DashboardController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('Supervisor');
	}

	public function index()
	{
		$solic = solicitudMantenimiento::count("solicitud_mantenimientos.id_Solicitud");
		$solicA = solicitudMantenimiento::where("solicitud_mantenimientos.Estado", "=" , "Abierto")->count();
		$solicB = solicitudMantenimiento::where("solicitud_mantenimientos.Estado", "=" , "Asignado")->count();
		$solicC = solicitudMantenimiento::where("solicitud_mantenimientos.Estado", "=" , "Pendiente")->count();
		$solicD = solicitudMantenimiento::where("solicitud_mantenimientos.Estado", "=" , "En proceso")->count();
		$solicE = solicitudMantenimiento::where("solicitud_mantenimientos.Estado", "=" , "En verificación")->count();
		$verifi = solicitudMantenimiento::where("solicitud_mantenimientos.Estado", "=" , "Verificado")->count();
		$prioridadA = solicitudMantenimiento::where("solicitud_mantenimientos.Prioridad", "=" , "1")->count();
		$prioridadB = solicitudMantenimiento::where("solicitud_mantenimientos.Prioridad", "=" , "2")->count();
		$prioridadC = solicitudMantenimiento::where("solicitud_mantenimientos.Prioridad", "=" , "3")->count();


		return view('dashboard.index', compact('prioridadA', 'prioridadB', 'prioridadC','solicA', 'solicB', 'solicC', 'solicD', 'solicE', 'solic','verifi'));
	}
}
