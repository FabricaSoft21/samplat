<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\solicitudMantenimiento;
use App\User;
use Carbon\Carbon;
use PDF;
use DB;

class ReporteController extends Controller
{
	public function reporte()
	{
		$datos = DB::table('users')
		->join("roles","users.Rol","=","roles.id_Rol")
		->where('users.Rol','=','2')
		->select("users.NombreCompleto AS NombreCom")
		->get();

		$casoTotal = solicitudMantenimiento::count("solicitud_mantenimientos.id_Solicitud");

		$Carbon = new Carbon();
		$hoy = Carbon::now()->format('d/m/Y');

		$pdf = \PDF::loadView('reporte.reportePDF', compact('datos','casoTotal'));
		//$this->pdf->set_options('tempDir','/tmp');
		return $pdf->download($hoy."-".'Reporte.pdf');
	}
}
