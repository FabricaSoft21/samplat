<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\categoria;
use App\Models\piso;
use App\Models\espacio;
use App\Models\edificio;
use App\Models\solicitudMantenimiento;
use App\Models\imagen;
use App\Models\insumo;
use App\Models\prioridad;
use App\Models\ficha_mantenimiento;
use App\Models\img_mantenimiento;
use App\Models\insumo_solicitud;
use App\Mail\SolucionesAgilesEnMantenimiento;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class SolicitudMantenimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitudes = solicitudMantenimiento::all();
        $categoria = categoria::all();
        $edificio = edificio::all();
        $piso = piso::all();
        $espacio = espacio::all();
        $insumo = insumo::all();
        $user = User::all();
        $prioridad = prioridad::all();
        $ficha = ficha_mantenimiento::all();
        $img = img_mantenimiento::all();
        $insumo = insumo::all();
        $insumoSol = insumo_solicitud::all();
        $i = 1;
        return view('solicitudes.index', compact('solicitudes', 'categoria', 'edificio', 'espacio', 'insumo','user', 'piso', 'prioridad', 'ficha', 'img', 'insumo', 'insumoSol', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitudes = new solicitudMantenimiento();
        $imagen = new imagen();
        $prioridad = prioridad::all();

        // Tabla imagenes
        $insert = null;

        if($request->hasfile('Imagen'))
        {
            foreach($request->file('Imagen') as $file)
            {
                $filename = time().$file->getClientOriginalName();
                $file->move(public_path().'/solicitudImg/', $filename);
                $insert[]['Imagen_1'] = $filename;
                $insert[]['Imagen_2'] = $filename;
                $insert[]['Imagen_3'] = $filename;
                $insert[]['Imagen_4'] = $filename;
                $insert[]['Imagen_5'] = $filename;
            }

            if (isset($insert['24'])) {
                $imagen = imagen::insert($insert['0']+$insert['6']+$insert['12']+$insert['18']+$insert['24']);

            }elseif (isset($insert['18'])) {
                $imagen = imagen::insert($insert['0']+$insert['6']+$insert['12']+$insert['18']);

            }elseif (isset($insert['12'])) {
                $imagen = imagen::insert($insert['0']+$insert['6']+$insert['12']);

            }elseif (isset($insert['6'])) {
                $imagen = imagen::insert($insert['0']+$insert['6']);

            }elseif (isset($insert['0'])) {
                $imagen = imagen::insert($insert['0']);
            }

        }else
        {
            $imagen->save();
        }

        // Tabla solicitud mantenimiento
        $solicitudes->NombreCompleto=$request->input('NombreCompleto');
        $solicitudes->Email=$request->input('Email');
        $solicitudes->Prioridad=$request->input('Prioridad');
        $solicitudes->Categoria=$request->input('Categoria');
        $solicitudes->Edificio=$request->input('Edificio');
        $solicitudes->Espacio=$request->input('Espacio');
        $solicitudes->Piso=$request->input('Piso');
        $solicitudes->Descripcion=$request->input('Descripcion');
        $datosImg = imagen::all()->last()->id_Imagen;
        $solicitudes->Imagen=$datosImg;
        $fecha = $request->input('Fecha_solicitud');
        $Fecha_solicitud = Carbon::createFromFormat('d/m/Y',$fecha);
        $solicitudes->Fecha_solicitud = $Fecha_solicitud->format('Y-m-d H:i:s');
        $timePrioridad = 0;
        if ($request->input('Prioridad') === "1")
        {
            $timePrioridad = prioridad::find(1)->Tiempo;
        }
        elseif ($request->input('Prioridad') === "2") {
            $timePrioridad = prioridad::find(2)->Tiempo;
        }
        elseif ($request->input('Prioridad') === "3") {
            $timePrioridad = prioridad::find(3)->Tiempo;
        }
        $solicitudes->Fecha_fin=Carbon::now()->addHour($timePrioridad)->format('Y-m-d H:i:s');
        $solicitudes->InsumoAsignado=0;
        $solicitudes->save();

        $mail = $request->input('Email');
        $solicitudes->Estado=$request->input('Estado');
        $data = $solicitudes;
        Mail::to($mail)->send(new SolucionesAgilesEnMantenimiento($data));

        // dd($solicitudes);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mant = solicitudMantenimiento::findOrFail($id);
        $prioridad = prioridad::all();

        $mant->Estado=$request->input('Estado');
        $mant->Tecnico=$request->input('Tecnico');
        $timePrioridad = 0;
        if ($mant->Prioridad === 1)
        {
            $timePrioridad = prioridad::find(1)->Tiempo;
        }
        elseif ($mant->Prioridad === 2) {
            $timePrioridad = prioridad::find(2)->Tiempo;
        }
        elseif ($mant->Prioridad === 3) {
            $timePrioridad = prioridad::find(3)->Tiempo;
        }
        $mant->Fecha_asignacion=Carbon::now()->addHour($timePrioridad)->format('Y-m-d H:i:s');
        $mant->save();

        $mail = $request->input('EmailUser');
        $mant->NombreTec=$request->input('NombreTec');
        $data = $mant;
        Mail::to($mail)->send(new SolucionesAgilesEnMantenimiento($data));
        
        // dd($mant);
        return redirect()->route('home');
    }

    public function updateTec(Request $request, $id)
    {
        $mant = solicitudMantenimiento::findOrFail($id);
        $prioridad = prioridad::all();

        $mant->Estado=$request->input('Estado');
        $mant->Tecnico=$request->input('Tecnico');
        $timePrioridad = 0;
        if ($mant->Prioridad === 1)
        {
            $timePrioridad = prioridad::find(1)->Tiempo;
        }
        elseif ($mant->Prioridad === 2) {
            $timePrioridad = prioridad::find(2)->Tiempo;
        }
        elseif ($mant->Prioridad === 3) {
            $timePrioridad = prioridad::find(3)->Tiempo;
        }
        $mant->Fecha_asignacion=Carbon::now()->addHour($timePrioridad)->format('Y-m-d H:i:s');
        $mant->save();

        $mail = $request->input('EmailUser');
        $mant->NombreTec=$request->input('NombreTec');
        $data = $mant;
        Mail::to($mail)->send(new SolucionesAgilesEnMantenimiento($data));
        
        // dd($mant);
        return redirect()->route('mantenimiento.index');
    }

    public function EstadoCaso(Request $request, $id)
    {
        $mant = solicitudMantenimiento::findOrFail($id);

        $mant->Estado=$request->input('Estado');
        $mant->save();

        $mail = $request->input('EmailUser');
        $mant->NombreTec=$request->input('NombreTec');
        $data = $mant;
        Mail::to($mail)->send(new SolucionesAgilesEnMantenimiento($data));
        
        // dd($mant);
        return redirect()->route('mantenimiento.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
