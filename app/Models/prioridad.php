<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class prioridad extends Model
{
	protected $table='prioridades';
	public $timestamps = false;
	protected $primaryKey = 'id_Prioridad';
	protected $fillable = [
		'Prioridad', 'Tiempo',
	];
}
