<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class insumo_solicitud extends Model
{
	protected $table='insumo_solicituds';

	public $timestamps = false;
	protected $primaryKey = 'id_Insumo_Solicitud';
	protected $fillable = [
		'id_Solicitud', 'id_Insumo', 'CantidadUsada',
	];  
}
