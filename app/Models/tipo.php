<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tipo extends Model
{
	protected $table='tipos';

	public $timestamps = false;
	protected $primaryKey = 'id_Tipo';
	protected $fillable = [
		'Tipo',
	];
}
