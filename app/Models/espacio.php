<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class espacio extends Model
{
	protected $table='espacios';

	public $timestamps = false;
	protected $primaryKey = 'id_Espacio';
	protected $fillable = [
		'Espacio',
	];
}
