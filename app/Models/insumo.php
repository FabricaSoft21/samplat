<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class insumo extends Model
{
	protected $table='insumos';

	public $timestamps = false;
	protected $primaryKey = 'id_Insumo';
	protected $fillable = [
		'Insumo', 'Cantidad',
	];
}
